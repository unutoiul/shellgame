import fs from 'fs';

module.exports = {
	writeFile: (filename, data) => {
		fs.appendFile(filename, JSON.stringify(data), (err) => {
	        if (err) {
	            throw err;
	        }
		});
	}
};

