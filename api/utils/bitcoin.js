import vars from './../lookups/vars.json';

const dec = vars.bitcoin.decimals;
const pow = Math.pow(10, dec);

module.exports = {
	validated: (bitcoin) => {
		let decimals = bitcoin.split('.')[1];

		if(decimals.length === dec){
			return true;
		}
		return false;
	},

	convertToInteger: (bitcoin) => {
		return Math.round( bitcoin * pow );
	},

	getAmount: (num) => {
    	return Number(num/pow).toFixed(dec);
	},

	getCurrency: (curr) => {
		return 'BTC';
	}
};