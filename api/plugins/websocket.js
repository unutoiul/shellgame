import nes from 'nes';
import Boom from 'boom';

import db from './../services/database/db';
import vars from './../lookups/vars.json';

import update from './../services/websocket/update';

import { getSocketId } from './../utils/string';

exports.register = (server, options, next) => {
	server.register({
	  	register: nes,
	  	options: {
            onMessage,
            onConnection, 
            onDisconnection
        }, function (err) {
            console.log('websocket ', err);
        }
    });

    next();
};

exports.register.attributes = {
    pkg: {
        name: 'websocket'
    }
};

const sockets = db.getSocket();
const rooms = db.getRooms();

const onDisconnection = (socket) => {

	let socketId = getSocketId(socket);
	delete sockets[socketId];

    // console.log('onDisconnection ' ,socketId);
	disconnectPlayer(socketId);
    onConnectionUpdate();

	// removePlayer(socketId);
};

const onConnection = (socket) => {
    sockets[getSocketId(socket)] = socket;
    onConnectionUpdate();
};

const onMessage = (socket, data, reply) => {
    console.log('onMessage', data);
    // sendRoomMessage(data.roomId, message);
};

// const sendRoomMessage = (roomId, message) =>{
//     let players = db.getRoom(roomId).players;

//     console.log('players ', roomId, players);

//     for (var i = 0; i < players.length; i++) {
//         var socketId = players[i].socketId;

//         db.getSocket[socketId].send(message, (err) => {
//             if(err){
//                 return err;
//             }
//         });
//     }
// }

const onConnectionUpdate = () => {
    //Send message to update users and rooms
    update.homepage();
}

const disconnectPlayer = (socketId) => {
	let room = db.disconnectedPlayer(socketId);
    let status = vars.status['player:disconnect'];

    if(!room) return;

    for(let i in room.players){
        db.setStatus(room.id, i, status);
        
        // console.log('disconnectPlayer', i);

		if(sockets[i]){
			sockets[i].send({name: 'updateRoom', data: room}, (err) => {
				if(err){
					console.log(err);
		            return err;
				}
			});
		}
	}
}

const removePlayer = (socketId) => {
	db.removePlayerDisconnected(socketId);

    let event = {
        name: 'updateRoom',
        data: db.getRooms()
    };

    // db.validateRooms();
    for (let i in sockets) {
        sockets[i].send(event , (err) => {
            if(err){
                console.log(err);
                return err;
            }
        });
    }
};