import authJWT from 'hapi-auth-jwt2';
import { getSocketId } from './../utils/string';

import Account from './../services/database/account/account';

const internals = {};

exports.register =  (server, options, next) => {
    server.register(authJWT, (err) => {
        if(err){
            return err;
        }

        server.auth.strategy('jwt', 'jwt', { 
            key: config.tls.key,
            validateFunc: validate,
            verifyOptions: { ignoreExpiration: true }
        });

        server.auth.default('jwt');
    });
    next();
};

exports.register.attributes = {
    pkg: {
        name: 'auth'
    } 
};

const validate = async (decoded, request, callback) => {
    // console.log('validate ', decoded);
    // console.log(request.info);
    // console.log(request.headers['user-agent']);

    // let socketId = getSocketId(request.socket);

    // console.log('socketId', socketId);

    // let valided = await Account.getData(decoded.id);

    // if(valided){
        return callback(null, true);
    // }else{
        // return callback(null, false);
    // }
};



