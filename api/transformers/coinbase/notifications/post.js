import { validated, convertToInteger } from './../../../utils/bitcoin';

const Obj = (data) => {
    let amount = data.additional_data.amount.amount;

    if(!validated(amount)){
    	return { amount: false };
    }

    return {
    	address: data.data.address,
    	currency: data.additional_data.amount.currency,
    	amount: convertToInteger(amount),
    	transactionId: data.additional_data.transaction.id,
    	type: data.type.split(':')[2],
    	time: data.created_at
    }
}

export default Obj;

      