import { convertToInteger } from './../../../utils/bitcoin';

const Obj = (data) => {
    return {
		date: new Date(), 
		transactionId: data.transactionId, 
		type: data.type, 
		status: data.status, 
		amount: data.amount, 
		address: data.address,
		currency: data.currency 
    }
}

export default Obj;

      