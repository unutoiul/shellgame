import Boom from 'boom';
import Hapi from 'hapi';
import Path from 'path';

import fs from 'fs';
import path from 'path';
import util from 'util';
import ejs from 'ejs';
import vision from 'vision';
import inert from 'inert';


import auth from './plugins/auth';
import websocket from './plugins/websocket';

import config from './config';

let api = new Hapi.Server();

let tls = {
    key: fs.readFileSync('./data/keys/csr.pem', 'utf8'),
    cert: fs.readFileSync('./data/keys/cwp-51.15.141.49.cert', 'utf8')
};
// let tls = {
//     key: fs.readFileSync('./data/keys/csr.pem', 'utf8'),
//     cert: fs.readFileSync('./data/keys/server.crt', 'utf8')
// };

api.connection({
  	port: config.port,
  	routes: {
        cors: true
  	},
   // tls: tls
});

//Register Plugins
api.register([auth, vision, inert, websocket], (err) => {
    if(err){
        return err;
    }
});

api.views({
    engines: {
        html: ejs
    },
    path: Path.join(__dirname, './../frontend', config.build)
});

api.subscription('/mainChat', { auth: false });
api.subscription('/room/{param}');

// Load routes directory recursively
let requireDir = (dir) => {
	for (let p of fs.readdirSync(dir)) {
		let fpath = path.resolve(dir, p);
		let stat = fs.lstatSync(fpath);
		if (stat.isDirectory()) {
			requireDir(fpath);
		} else if (stat.isFile()) {
			let route = require(fpath);
			api.route(route);
		}
	}
};

requireDir(path.join(__dirname, '/routes'));

//Show all routes
// var table = api.table();
// for (var i = 0; i < table[0].table.length; i++) {
//     console.log(table[0].table[i].path);
// }

export default api;
