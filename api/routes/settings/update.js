import Boom from 'boom';

const Route = {
  	method: 'PUT',
  	path: '/api/settings/update',
  	handler: (request, reply) => {
		let username = request.username;

        reply({
        	'data': User.getData(username)
        })
        .header("Authorization", request.headers.authorization);
  	}
};

export default Route;