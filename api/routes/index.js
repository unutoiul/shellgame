import Path from 'path';

const Route = {
 	method: 'GET',
	path: '/',
	config:{
		auth: false
	},
	handler: (request, reply) => {
		return reply.view('index');
  	}
};

export default Route;