import Boom from 'boom';

import shortid from 'shortid';
import db from './../../services/database/db';

import Account from './../../services/database/account/account';
import email from './../../services/email/send-email';

import { getSocketId } from './../../utils/string';

const Route = {
    method: 'POST',
    path: '/api/support',
    handler: async function (request, reply) {
        let credentials = request.auth.credentials;
        let data = request.payload;
        // let server = request.connection.server;
        // let socketId = getSocketId(request.socket);

        let account = await Account.getData(credentials.id);

        if(!account.email){
            Boom.badRequest('Please update your email address in order to send us a question.');
        }

        let from = account.email;
        let to = config.email;
        
        data.email = account.email;         

        try{
            var emailStatus = await email(from, to, 'Email send from support of shellgame.net', 'support', data);
        }catch(e){
            return reply(Boom.badRequest(e));
        }

        console.log(emailStatus);

        return reply(emailStatus);
    }
};

export default Route;