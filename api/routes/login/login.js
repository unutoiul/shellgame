import Boom from 'boom';
import JWT from 'jsonwebtoken';

import Account from '../../services/database/account/account';

const Route = {
  	method: 'POST',
  	path: '/api/login',
	config: {
		auth: false
  	},
  	handler: async (request, reply) => {
	  	let email = (request.payload)?request.payload.email:null;
	  	let password = (request.payload)?request.payload.password:null;
	  	
	  	if(!email || !password){
	  		return reply(Boom.badRequest('Username or password are missing.'));
	  	}

		let auth = await Account.verify(email, password);

		if(!auth){
			return reply(Boom.unauthorized('Username or password are not correct.'))
		}
		
		let token = JWT.sign({id: auth.id, name: auth.fullName}, config.tls.key);

		return reply({
			token: token
		})

  	}
};

export default Route;