import Boom from 'boom';
import requestHttp from 'request';

import moment from 'moment';

import Account from '../../services/database/account/account';
import updateBySocket from './../../services/websocket/update';

const Route = {
    method: 'POST',
    path: '/api/faucet',
    handler: async (request, reply) => {
        let credentials = request.auth.credentials;
        let data = request.payload;

        let options = {
            method: 'POST',
            uri: config.recaptcha.verify,
            qs: {
                secret: config.recaptcha.secretKey,
                response: data.myRecaptchaResponse
            }
        };

        let account = await Account.getData(credentials.id);

        if(account.faceutAt){
            
            console.log(account.faceutAt.format("HH:mm:ss"));
            console.log(moment().format("HH:mm:ss"));
            console.log(account.faceutAt < moment());


            if(moment(account.faceutAt) < moment()){
                return reply(Boom.badRequest('You need to wait.'));
            }
        }      

        requestHttp(options, async (error, response, body)  => {
            if(error){
                return reply(Boom.badRequest(error));
            }

            let transaction;
            let amount = 150;
            let faceutAt = moment().add(config.recaptcha.timeout, 'm');

            //Update wallet
            try{
                transaction = await Account.updateWallet(credentials.id, amount);
            }catch(e){
                return reply(Boom.badRequest(e.message));
            }      

            //Update faucet time
            try{
                transaction = await Account.updateFaucet(credentials.id, faceutAt);
            }catch(e){
                return reply(Boom.badRequest(e.message));
            }

           //Find socket if connected
            try{
                updateBySocket.sendByAccountId(String(credentials.id), transaction);
            }catch(e){
                console.log(e.message);
                return reply(Boom.badRequest(e.message));
            }
            
            return reply({data:body, transaction: transaction});
        });
    }
};

export default Route;