import Boom from 'boom';

import Account from '../../services/database/account/account';

const Route = {
    method: 'GET',
    path: '/api/count',
    config: {
	  	auth: false
  	},
    handler: async (request, reply) => {
        let count = await Account.count();
        
        return reply({count:count});
        // .header("Authorization", request.headers.authorization);
    }
};

export default Route;