import Boom from 'boom';

import Account from '../../services/database/account/account';

const Route = {
  	method: 'GET',
  	path: '/api/account',
  	handler: async (request, reply) => {
        let credentials = request.auth.credentials;
        let data = await Account.getData(credentials.id);

        return reply(data);
  	}
};

export default Route;