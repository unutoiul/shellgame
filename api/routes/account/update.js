import Boom from 'boom';
import validator from 'validator';

import Account from '../../services/database/account/account';
import lookups from './../../lookups/vars';

const Route = {
  	method: 'PUT',
  	path: '/api/account',
  	handler: async (request, reply) => {
        let credentials = request.auth.credentials;
        let data = request.payload;
        let emailDuplicated, userData;

        // console.log('user ' , user);

        let emailValid = validator.isEmail(data.email);
        let messsage = (data.update) ? lookups.messages.success.setAccount:lookups.messages.success.updateAccount;

        if(!emailValid){
            return reply(Boom.conflict('Email is not in valid format.'))
        }
        
        //Check if email is duplicated
        try{
            if(data.update){
                emailDuplicated = await Account.emailDuplicated(data.email);
                if(emailDuplicated) {
                    return reply(Boom.conflict('Email is already registered.'));
                }
            }
        }catch(e){
            return reply(handleError(e));
        }

        let user = {
        	email: data.email,
        	password: data.password
        };

        try{
	        userData = await Account.update(credentials.id, user);
        }catch(e){
            return reply(handleError(e));
        }

        console.log('success setAccount ', lookups.messages.success.setAccount);

        return reply({
            message: messsage,
            data: userData
        });
        // .header("Authorization", request.headers.authorization);
  	}
};

export default Route;