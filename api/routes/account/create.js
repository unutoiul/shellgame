import Boom from 'boom';
import JWT from 'jsonwebtoken';
import validator from 'validator';

import Account from './../../services/database/account/account';
import Coinbase from './../../services/coinbase/';

import sendEmail from '../../services/email/send-email';

const handleError = (err) =>{ 
    console.log(err);
    return Boom.badRequest(err.message);
}

const Route = {
  	method: 'POST',
  	path: '/api/create-account',
    config: {
		  auth: false
  	},
  	handler: async (request, reply) => {
  		let user = request.payload;
        let accountId = config.coinbase.accountID;
        let emailDuplicated, walletData, userData;

        let emailValid = validator.isEmail(user.email);
        if(!emailValid){
            return reply(Boom.conflict('Email is not in valid format.'))
        }
        
        //Check if email is duplicated
        try{
            emailDuplicated = await Account.emailDuplicated(user.email);
            if(emailDuplicated) {
                return reply(Boom.conflict('Email is already registered.'));
            }
        }catch(e){
            return reply(handleError(e));
        }

        //Get a new wallet address
        try{
            walletData = await Coinbase.createAddress(accountId);
            walletData = JSON.parse(walletData);
            
            user.createdAt = new Date();
            
            user.wallet = walletData;
            user.wallet.totalAmount = 0;
            user.wallet.currency = 'satoshi';

        }catch(e){
            return reply(handleError(e));
        }

        //Save data in MongoDB
        try{
            userData = await Account.create(user);
        }catch(e){
            return reply(handleError(e));
        }

        let token = JWT.sign({id: userData.id, name: userData.fullName}, config.tls.key);

		return reply({
			// emailStatus: emailStatus,
			userData: userData,
            token: token
		});

  	}
};

export default Route;