import Mongo from  './../../services/database/mongo/mongo_db';

const Route = {
  	method: 'POST',
  	path: '/api/find-account',
  	handler: async (request, reply) => {
  		let name = request.payload.name;
  		let schema =  { $or :[
  			{firstname: { $regex : name, $options : 'i' }},
  			{lastname: { $regex : name, $options : 'i' }},
  			{email: { $regex : name, $options : 'i' }}
  		]};
		var userData = await Mongo.find('users', schema);

		return reply({
			results: userData
		});
  	}
};

export default Route;