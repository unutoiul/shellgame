import Boom from 'boom';
import JWT from 'jsonwebtoken';
import validator from 'validator';

import Account from './../../services/database/account/account';
import Coinbase from './../../services/coinbase/';

import sendEmail from '../../services/email/send-email';

const handleError = (err) =>{ 
    return Boom.badRequest(err.message);
}

const Route = {
  	method: 'POST',
  	path: '/api/join',
    config: {
		  auth: false
  	},
  	handler: async (request, reply) => {
  		let user = request.payload;
        let accountId = config.coinbase.accountID;
        let walletData, userData, nameDuplicated;

        //Check if email is duplicated
        try{
            nameDuplicated = await Account.nameDuplicated(user.fullName);
            if(nameDuplicated) {
                return reply(Boom.conflict('Name already taken.'));
            }
        }catch(e){
            return reply(handleError(e));
        }

        //Get a new wallet address
        try{
            walletData = await Coinbase.createAddress(accountId);
            walletData = JSON.parse(walletData);
            
            user.createdAt = new Date();
            
            user.wallet = walletData;
            user.wallet.totalAmount = 0;
            user.wallet.currency = 'satoshi';
        }catch(e){
            return reply(handleError(e));
        }

        //Save data in MongoDB
        try{
            userData = await Account.create(user);
        }catch(e){
            return reply(handleError(e));
        }

        let token = JWT.sign({id: userData.id, name: userData.fullName}, config.tls.key);

		return reply({
			// emailStatus: emailStatus,
			userData: userData,
            token: token
		});
  	}
};

export default Route;