import Path from 'path';

const Route = {
    method: 'GET',
    path: '/partials/{param*}',
    config: {
        auth: false
    },
    handler: {
        directory: {
            path: Path.join(__dirname, config.frontend, config.partials),
            index: false
        }
    }
};

export default Route;