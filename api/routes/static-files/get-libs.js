import Path from 'path';

const Route = {
    method: 'GET',
    path: '/libs/{param*}',
    config: {
        auth: false
    },
    handler: {
        directory: {
            path: Path.join(__dirname, config.frontend, config.libs),
            index: false
        }
    }
};

export default Route;