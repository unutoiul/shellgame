import Path from 'path';

const Route = {
    method: 'GET',
    path: '/{param*}',
    config: {
        auth: false
    },
    handler: (request, reply) => {
        return reply.view('index');
    }
};

export default Route;