import Path from 'path';

const Route = {
    method: 'GET',
    path: '/assets/{param*}',
    config: {
        auth: false
    },
    handler: {
        directory: {
            path: Path.join(__dirname, config.frontend, config.build),
            index: false
        }
    }
};

export default Route;