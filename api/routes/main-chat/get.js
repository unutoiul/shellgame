import { getSocketId } from './../../utils/string';

import db from './../../services/database/db.js';

const Route = {
    method: 'GET',
    path: '/api/messages',
    handler: function (request, reply) {
        let server = request.connection.server;
        let messages = db.getChatMessages();

        return reply(messages);
    }
};

export default Route;