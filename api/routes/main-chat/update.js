import { getSocketId } from './../../utils/string';

import db from './../../services/database/db.js';

const Route = {
    method: 'PUT',
    path: '/api/messages',
    handler: function (request, reply) {
        let server = request.connection.server;
        let socketId = getSocketId(request.socket);
     	let credentials = request.auth.credentials;
        let message = request.payload;

        message.from = credentials.name;

        db.addChatMessssage(message);

        server.publish('/mainChat', message);

        return reply(message);
    }
};

export default Route;