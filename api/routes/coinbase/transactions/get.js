import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

import _ from 'lodash';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/address/transactions',
	config: {
		auth: false
  	},
  	handler: (request, reply) => {
  		var accountId = config.coinbase.accountID;
		var addressId = '52a5873e-4ebf-5f90-ac94-622ce61b7fa4';
		// var addressId = '2e792294-ea17-5f26-8c66-0179022e8200';
		// var address = '16azcxhKiFTr7X3ziLEhtDgpzTo42KeRMR';

		var arr = [];

		console.log('accountId ', accountId);
		console.log('addressId ', addressId);

		coinbase.getTransactions(accountId, addressId, function(err, data){
			if(err){
				return reply(err);
			}

			for (var i = 0; i < data.length; i++) {
				arr.push(JSON.parse(data[i]));
			}
		
			return reply(arr);
		});
  	}
};

export default Route;