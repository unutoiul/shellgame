import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/address/all',
	config: {
		auth: false
  	},
  	handler: async (request, reply) => {
		var accountId = config.coinbase.accountID;

		console.log('accountId ', accountId);

		try{
			var addresses = await coinbase.getAddresses(accountId);
		}catch(e){
			return reply(Boom.badRequest(e.message));
		}

		var arr = [];
		addresses.forEach(function(item){
			arr.push({
				address: item.address,
				created_at: item.created_at,
				updated_at: item.updated_at
			});
		})

		return reply(arr);
  	}
};

export default Route;