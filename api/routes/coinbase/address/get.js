import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/wallets',
	config: {
		auth: false
  	},
  	handler: (request, reply) => {

  		coinbase.getAccounts(function(err, accounts){
  			if(err){
  				return reply(err);
  			}

			return reply(JSON.parse(accounts));
  		});
  	}
};

export default Route;