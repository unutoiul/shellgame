import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/address/create',
	config: {
		auth: false
  	},
  	handler: async (request, reply) => {
		var accountId = config.coinbase.accountID;

		try{
			var address = await coinbase.createAddress(accountId);
		}catch(e){
			return reply(e.message);
		}

    	return reply(JSON.parse(address));
  	}
};

export default Route;