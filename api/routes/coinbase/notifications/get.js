import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/notifications',
	config: {
		auth: false
  	},
  	handler: (request, reply) => {

  		coinbase.getNotifications(function(err, notifications){
  			if(err){
  				return reply(err);
  			}

            var arr = [];
            notifications.forEach(function(item){
                arr.push(JSON.parse(item));
            });

			return reply(arr);
  		});
  	}
};

export default Route;