import Boom from 'boom';
import fs from 'fs';

import vars from './../../../lookups/vars.json';

//Transforms
import notificationsTransfrom from './../../../transformers/coinbase/notifications/post';

//Utils
import { writeFile } from './../../../utils/file';

//Services
import db from './../../../services/database/db';
import Account from './../../../services/database/account/account';
import Coinbase from './../../../services/coinbase/index';

import updateBySocket from './../../../services/websocket/update';


import { getSocketId } from './../../../utils/string';


const Route = {
  	method: 'POST',
  	path: '/api/coinbase/notifications',
    config: {
		auth: false
  	},
    handler: async (request, reply) => {
        let server = request.connection.server;
        let payload = request.payload;
        let headers = request.headers;

        let notification = notificationsTransfrom(payload);
        let transaction, account, amount, socket, room, socketId;

        if(!notification.amount){
        	writeFile(config.notifications,{ error: vars.messages.error.btcNotValid });

        	return reply(Boom.badRequest(vars.messages.error.btcNotValid));
        }

        switch(notification.type){
            case 'new-payment':
            notification.type = 'deposit';
            notification.status = 'completed';
            amount = notification.amount;
            break;

            default:
            amount = 0;
            break;
        }

        if(config.env === 'prod'){
            if(!headers['cb-signature']){
               writeFile(config.notifications,{ error: vars.messages.error.signatureNotFound });
               return reply(Boom.badRequest(vars.messages.error.signatureNotFound));
            }

            if (!Coinbase.verifyCallback(payload, headers['cb-signature'])) {
                writeFile(config.notifications, {error: vars.messages.error.signatureNotValid});
               return reply(Boom.badRequest(vars.messages.error.signatureNotValid));
            }
        }


   		writeFile(config.notifications, {headers:headers, payload:payload});

        //get account data
        try{
           account = await Account.getDataByAddress(payload.data.address);
        }catch(e){
            return reply(Boom.badRequest(e.message));
        }  

        if(!account){
            return reply(Boom.badRequest(vars.messages.error.accountNotFound));
        }

        //Add transaction
        try{
            transaction = await Account.addTransaction(account._id, notification);
        }catch(e){
            return reply(Boom.badRequest(e.message));
        } 

        //Update wallet
        try{
        	transaction = await Account.updateWallet(account._id, amount);
    	}catch(e){
        	return reply(Boom.badRequest(e.message));
        }

        //Find socket if connected
        try{
            updateBySocket.sendByAccountId(String(account._id), transaction);
        }catch(e){
            console.log(e.message);
            return reply(Boom.badRequest(e.message));
        }

        return reply(transaction);
  	}
};

export default Route;