import Boom from 'boom';

import coinbase from './../../../services/coinbase/';

const Route = {
  	method: 'GET',
  	path: '/api/coinbase/account',
	config: {
	   auth: false
  	},
  	handler: (request, reply) => {
        try{
  		    var accounts = coinbase.getAccounts();
        }catch(e){
            return reply(e.message);
        }

        var arr = [];
        // accounts.forEach(function(item){
        //     arr.push(item);
        // });
        
        return reply(accounts);
  	}
};

export default Route;