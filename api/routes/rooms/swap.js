import Boom from 'boom';
import vars from './../../lookups/vars.json';
import db from './../../services/database/db';
import ws from './../../services/websocket/ws';
import update from './../../services/websocket/update';
import { getSocketId } from './../../utils/string';

const Route = {
    method: 'GET',
    path: '/api/swapPlayers/{roomId}',
    handler: function (request, reply) {
        let server = request.connection.server;
        let credentials = request.auth.credentials;
        let socketId = getSocketId(request.socket);
        let roomId = request.params.roomId;

        if(!db.getRoom(roomId)){
            return reply(Boom.locked('Room doesn\'t exist'));
        }

        let room = db.swapPlayers(roomId);

        if(room){
            ws.sendToPlayers(room.players, { name:'updateRoom', data: room }, function(err){
                if(err){
                    return reply(err);            
                }
            });
        }

        return reply(room);
    }
};

export default Route;