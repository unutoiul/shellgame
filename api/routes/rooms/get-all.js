import db from './../../services/database/db';

import { getSocketId } from './../../utils/string';

const Route = {
    method: 'GET',
    path: '/api/rooms',
    handler: function (request, reply) {
        let data = db.getRooms();
        return reply(data);
    }
};

export default Route;