import Boom from 'boom';

import db from './../../services/database/db';
import vars from './../../lookups/vars.json';
import websocket from './../../services/websocket/ws';
import { getSocketId } from './../../utils/string';

const Route = {
    method: 'GET',
    path: '/api/room/{id}',
    handler: function (request, reply) {
        let roomId = request.params.id;
        let credentials = request.auth.credentials;
     	let socketId = getSocketId(request.socket);
        let server = request.connection.server;
        let room = db.getRoom(roomId);

        if(!roomId) {
        	return reply(Boom.badRequest('Room id does\'t exist.'));
        }

    	if(!room){
    		return reply(null);
    	}

    	//try to reconnect player
	 	let data = db.reconnectPlayer(roomId, socketId, credentials);

        if(data){
            for (let socketId in room.players) {
                let host = room.players[socketId].host;
                let numPlayers = Object.keys(room.players).length;

                let status = (numPlayers === 2) ? vars.status['player:connected'] : vars.status['player:waiting'];

                db.setStatus(roomId, socketId, status);

                // console.log('numPlayers', numPlayers);
                
                websocket.send(socketId, { name:'updateRoom', data: room});

                // if connected send message
                if(numPlayers === 2){
                    let newStatus = (host)? vars.status['game:host:start'] : vars.status['game:player:wait'];
                    
                    db.setStatus(roomId, socketId, newStatus);

                    websocket.send(socketId, { name:'updateRoom', data: room});
                }
            }
        }
        // server.publish(`/room/${roomId}`, data);
	 	
 		return reply(data);
    }
};

export default Route;