import Boom from 'boom';

import db from './../../services/database/db';
import vars from './../../lookups/vars.json';
import websocket from './../../services/websocket/ws';
import { getSocketId } from './../../utils/string';


const Route = {
    method: 'GET',
    path: '/api/disconnected/{roomId}',
    handler: function (request, reply) {
        let server = request.connection.server;
        let credentials = request.auth.credentials;
        let socketId = getSocketId(request.socket);
        let roomId = request.params.roomId;

        if(!roomId){
            return replay(Boom.badRequest());
        }

        let room = db.removePlayer(roomId, socketId);
        
        websocket.sendToPlayers(room.players, {name:'updateRoom', data: room}, function(err){
            if(err){
                return reply(err);            
            }
        });

        // if(room){
        //     server.publish(`/room/${roomId}`, room);
        // }

        // server.publish('/room/'+roomId, room);
        // server.broadcast(db.getRoom());
        
        return reply(room);
    }
};

export default Route;