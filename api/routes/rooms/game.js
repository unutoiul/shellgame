import _ from 'lodash';
import Boom from 'boom';

import db from './../../services/database/db';
import vars from './../../lookups/vars.json';

import websocket from './../../services/websocket/ws';

import Account from './../../services/database/account/account';

import { validated, getAmount, convertToInteger } from './../../utils/bitcoin';
import { getSocketId } from './../../utils/string';

const Route = {
    method: 'PUT',
    path: '/api/game/{roomId}',
    handler: async function (request, reply) {
        let server = request.connection.server;
        let roomId = request.params.roomId;
        let data = request.payload;
     	let socketId = getSocketId(request.socket);
        let room = db.getRoom(roomId);
        let status;

        let requestStatus = room.players[socketId].status;
        let waitCodes = [10, 22, 23, 25, 26, 27, 28];

        let resetGame = function (){
            db.swapHost(roomId);

            //set status
            for(let i in room.players){
                let host = room.players[i].host;    
                let status = (host) ? vars.status['game:host:start'] : vars.status['game:player:wait'];
                db.setStatus(roomId, i, status);
            }

            websocket.sendToPlayers(room.players, {name: 'updateRoom', data:room});

        };

        if(waitCodes.indexOf(requestStatus.code) > -1){
            return reply(Boom.badRequest('The request is not accepted.'));
        }

        //Create game
        if(requestStatus.code === 21){
            data.betAmount = getAmount(convertToInteger(data.betAmount));

            db.createGame(roomId, data);

            for(let i in room.players){
                let host = room.players[i].host;    
                status = (host) ? vars.status['game:host:wait'] : vars.status['game:player:start'];
                db.setStatus(roomId, i, status);
            }
            
            //remove cupId from return
            room = _.cloneDeep(room);
            delete room.game.cupId;
        }

        //Check game
        if(requestStatus.code === 24){
            let valid = db.checkGame(data.cupId, roomId);
            let betAmount = convertToInteger(room.game.betAmount);

            for(let i in room.players){
                let host = room.players[i].host;
                let id = room.players[i].id;
                let amount, account;

                // console.log('id---' , id, betAmount);

                if(valid){
                    status = (host) ? vars.status['game:host:lost'] : vars.status['game:player:win'];
                    amount = (host) ? -betAmount : betAmount;
                }else{
                    status = (host) ? vars.status['game:host:win'] : vars.status['game:player:lost'] ;
                    amount = (host) ? betAmount : -betAmount;
                }

                try{
                    account = await Account.updateWallet(id, amount);
                }catch(e){
                    return reply(Boom.badRequest(e));
                }

                db.setWallet(roomId, i, account.wallet.totalAmount);
                db.setStatus(roomId, i, status);

                websocket.send(i, {name: 'updateUser', data: account});
            }
            setTimeout(resetGame, 5000);
        }

        // console.log(room);

        websocket.sendToPlayers(room.players, {name: 'updateRoom', data:room});

        return reply(room);
    }
};

export default Route;