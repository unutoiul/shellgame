import shortid from 'shortid';
import db from './../../services/database/db';

import Account from './../../services/database/account/account';
import Update from './../../services/websocket/update';

import { getSocketId } from './../../utils/string';


const Route = {
    method: 'POST',
    path: '/api/find-a-room',
    handler: async function (request, reply) {
        let server = request.connection.server;
        let credentials = request.auth.credentials;
        let socketId = getSocketId(request.socket);
        let account;

        // credentials.id = '58ff2c1c491d750b87f6014a';

        try {
            account = await Account.getData(credentials.id);
        } 
        catch(err) {
            return reply(err);
        }

        credentials.socketId = socketId;
        credentials.host = false;
        credentials.totalAmount = account.wallet.totalAmount;
        
        let room = db.findARoom(credentials);
        
        if(!room){
            credentials.host = true;

            let data = {
                id: shortid.generate(),
                players: {}
            };

            data.players[socketId] = credentials;
           
            room = db.createRoom(data);
        }

        //Send message to update users and rooms
        Update.homepage();

        return reply(room);
    }
};

export default Route;