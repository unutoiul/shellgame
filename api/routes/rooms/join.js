import db from './../../services/database/db';
import Boom from 'boom';

import { getSocketId } from './../../utils/string';

const Route = {
    method: 'GET',
    path: '/api/join-room/{roomId}',
    handler: function (request, reply) {
        let server = request.connection.server;
        let roomId = request.params.roomId;
        let credentials = request.auth.credentials;
        let socketId = getSocketId(request.socket);
        let room = db.getRoom(roomId);

        credentials.socketId = socketId;
        credentials.host = false;

        for(let socketId in room.players){
        	if(room.players[socketId].username === credentials.username){
            	return reply(Boom.locked('You can\'t join the game you host.'));
        	}
        }

        if(Object.keys(room.players).length >= 2){
            return reply(Boom.locked('Game is already started.'));
        };

        let newRoom = db.addPlayer(roomId, socketId, credentials);

        // server.publish('/room/'+roomId, newRoom);
        // server.broadcast(db.getRooms());

        return reply(newRoom);
    }
};

export default Route;