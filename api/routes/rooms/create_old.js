import shortid from 'shortid';
import db from './../../services/database/db';
import update from './../../services/websocket/update';


import { getSocketId } from './../../utils/string';

const Route = {
    method: 'POST',
    path: '/api/create-room',
    handler: function (request, reply) {
        let server = request.connection.server;
        let credentials = request.auth.credentials;
        let data = request.payload;
        
        let socketId = getSocketId(request.socket);

        credentials.socketId = socketId;
        credentials.host = true;

        // console.log('request.auth.credentials create', request.auth.credentials);
        // console.log('socket.id', request.socket.id);
        // console.log('request.auth.credentials.username',request.auth.credentials.username);
        // let userData = User.getData(username);
        // User.getData(request.username);

        data.players[socketId] = credentials;
        data.id = shortid.generate();

        let joinRoom = db.joinRoom(data);

        if(!joinRoom){
            let room = db.createRoom(data);
            return reply(room);
        }

        //Send message to update users and rooms
        update.homepage();

        return reply(joinRoom);
    }
};

export default Route;