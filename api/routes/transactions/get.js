import Boom from 'boom';

import Account from '../../services/database/account/account';

const Route = {
  	method: 'GET',
  	path: '/api/transactions/{status?}',
  	handler: async (request, reply) => {
        let credentials = request.auth.credentials;
        let status = request.params.status;

        // console.log('status ', status);

        try{
            let data = await Account.getTransactions(credentials.id, status);
            return reply(data);
        }
        catch(e){
            return reply(e);
        }
  	}
};

export default Route;