import Boom from 'boom';
import shortid from 'shortid';

import Account from '../../services/database/account/account';
import updateBySocket from './../../services/websocket/update';

import { getAmount, validated, convertToInteger } from './../../utils/bitcoin';

const Route = {
  	method: 'POST',
  	path: '/api/transactions',
  	handler: async (request, reply) => {
        let credentials = request.auth.credentials;
        let data = request.payload;
        let type = request.params.type;

        if(data.type == 'withdraw'){
             try{
                // let validateAmount = validated(data.amount);
                // if(!validateAmount){
                //     return reply(Boom.badRequest('Bitcoin value is not in the right format.'));
                // }

                let account = await Account.getData(credentials.id);
                let valid = (account.wallet.totalAmount >= data.amount);
                if(!valid){
                    return reply(Boom.badRequest('Not enough bitcoins in your wallet.'));
                }


        console.log('data.amount' , data.amount);
                data.transactionId = shortid.generate();
                data.currency = 'satoshi';
                data.status = 'pending';
                data.amount = -convertToInteger(data.amount);

            }
            catch(e){
                return reply(e);
            }

            //Update wallet
            try{
                var updateWallet = await Account.updateWallet(credentials.id, data.amount);
            }
            catch(e){
                return reply(e);
            }
        }

        console.log('data.amount' , data.amount);
        // console.log(updateWallet);
        
        //Add Transaction 
        try{
            var transaction = await Account.addTransaction(credentials.id, data);
        }
        catch(e){
            return reply(e);
        }

        // console.log('transactions' , transaction);
        
        //Update users
        updateBySocket.sendByAccountId(credentials.id , updateWallet);



    	return reply(transaction);
        // .header("Authorization", request.headers.authorization);
  	}
};

export default Route;