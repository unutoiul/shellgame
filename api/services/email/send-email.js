import Path from 'path';

import lodash from 'lodash';
import nodemailer from 'nodemailer';

import fs from 'fs';

module.exports = (from, to, subject, template, args) => {
	return new Promise((resolve, reject) => {

		if (!to || !subject) {
			return reject('variables to and subject should not be empty');
		}

		try {
			var templatePath = Path.join(__dirname, '../../templates/email/' + template + '.html');
			var templateFilePath = fs.readFileSync(templatePath, 'utf8');
			var email = lodash.template(templateFilePath)(args);
		} catch (error) {
			console.log('api.email', 'email process template error', error);
			return reject(error);
		}

		var client = null, options = {};

		// var from = config.smtp.from;

		console.log(from);
		// console.log(client);

		// SMTP via standard server
		options = {
			host: config.smtp.server,
			secure: config.smtp.secure,
			port: config.smtp.port,
			auth: {
				user: config.smtp.username,
				pass: config.smtp.password
			},
			tls: {
        		rejectUnauthorized: false
    		}
		};

		client = nodemailer.createTransport(options);

		// Send the email
		client.sendMail({
			from: from + ' <' + from + '>',
			to: to,
			subject: subject,
			html: email
		}, (error, result) => {
			if (error) {
				console.log('api.email', 'nodemailer error when sending email', error);
				return error;
			}
			error ? reject(error) : resolve(result);
		});
	});
};
