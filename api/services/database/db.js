import _ from 'lodash';
import vars from './../../lookups/vars.json';

const rooms = {};
const sockets = {};
const messages = [];

const db = {
	createRoom: (room) => {
		rooms[room.id] = room;

		return room;
	},

	findARoom: (data) => {
		for(let roomId in rooms){
			let room = rooms[roomId];

			if(Object.keys(room.players).length < 2){
				let players = room.players;

				//check if is not same player who created the game
				//check if room is frozen
				for(let player in players){
					if(players[player].id === data.id && !room.frozen){
						return null;
					}
				}

				players[data.socketId] = data;
				
				return room;
        	};
		}

		return null;
	},

	getRooms: () => {
		return rooms;
	},

	getRoom: (id) => {
		if(!rooms[id]){
			return null;
		}

		return rooms[id];
	},	

	getSocket: (id) => {
		if(id){
			if(!sockets[id]){
				return null;
			}
			return sockets[id];
		}

		return sockets;
	},

	addChatMessssage: (message) => {
		messages.push(message);

		return messages;
	},

	getChatMessages: () => {
		return messages;
	},

	addPlayer: (roomId, socketId, player) => {
		rooms[roomId].players[socketId] = player;
		return rooms[roomId];
	},

	reconnectPlayer(roomId, socketId, player){
		if(!rooms[roomId]) return null;

		let players = rooms[roomId].players;

		if(Object.keys(players).length === 2){
			for (let i in players) {
				if(players[i].id === player.id){

					//Set variables from old connection
					player.host = players[i].host;
					player.totalAmount = players[i].totalAmount;
					player.socketId = socketId;
					rooms[roomId].frozen = false;

					//Delete old connection
					delete players[i];
					
					return db.addPlayer(roomId, socketId, player);
				}
			}
		}else{
			for (let i in players) {
				//check if player was already in the room
				if(players[i].id === player.id){
					return rooms[roomId];
				}
			}
		}
		return null;
	},

	disconnectedPlayer: (socketId) => {
		for (let i in rooms) {
			for (let j in rooms[i].players){
				if(rooms[i].players[j].socketId === socketId){
					if((Object.keys(rooms[i].players)).length < 2){
						delete rooms[i];
						return null;
					}

					rooms[i].frozen = true;
					return rooms[i];
				}
			}
		}
		return null;
	},

	removePlayer: (roomId, socketId) => {
		let players = rooms[roomId].players;

		for (let id in players) {
			if(id === socketId){
				if(rooms[roomId].game){
					delete rooms[roomId].game;
				}
				delete rooms[roomId].players[id];
			}else{
				players[id].host = true;
				players[id].status = vars.status['player:waiting'];
			}
		}

		if(_.isEmpty(players)){
			delete rooms[roomId];
			return null;
		}
		return rooms[roomId];
	},

	createGame: (roomId, game) => {
		rooms[roomId].game = game;
		return game;
	},

	swapPlayers: (roomId) => {
		let players = rooms[roomId].players;
		for(let i in players){
			players[i].host = !players[i].host;

			if(players[i].host){
				players[i].status = vars.status['game:host:start']
			}else{
				players[i].status = vars.status['game:player:wait']
			}
			
		}
		//clear game
		rooms[roomId].game = null;
		
		return rooms[roomId];
	},
	
	setWallet: (roomId, socketId, totalAmount) => {
		rooms[roomId].players[socketId].totalAmount = totalAmount;
		return rooms[roomId];
	},
	
	setStatus: (roomId, socketId, status) => {
        rooms[roomId].players[socketId].status = status;
        return rooms[roomId];
	},

 	checkGame: (cupId, roomId) => {
 		rooms[roomId].game.cupSelected = cupId;
	    if(rooms[roomId].game.cupId == cupId){
	        return true;
	    };
	    return false;
	},

	updateRoomWallet: (socketId, transaction) => {
		let roomIds = [];
		for(let i in rooms){
			if(rooms[i].players[socketId]){
				rooms[i].players[socketId].totalAmount = transaction.wallet.totalAmount;
				roomIds.push(i);
			}
		}

		return roomIds;
	},

	updatePlayers: (room) => {
    	rooms[room.id].players = room.players;
        return room;
	},
	
	findSocket: (accountId) => {
		for(let i in sockets){
			let id = sockets[i].auth.credentials.id;
			if(id === accountId){
				return sockets[i];
				break;
			}
		}

		return null;
	}
}


export default db;