import mongoose from 'mongoose';
import config from './../../../config';

const connect = () => {
	let url = `mongodb://${config.mongodb.user}:${config.mongodb.password}@${config.mongodb.url}:${config.mongodb.port}/${config.mongodb.database}`;
	let shell = `${config.mongodb.url}:${config.mongodb.port}/${config.mongodb.database} -u ${config.mongodb.user} -p ${config.mongodb.password}`;
	let db = mongoose.connection;

	let options = {
		useMongoClient: true,
		keepAlive: 120, 
		connectTimeoutMS: 30000,
		reconnectTries: Number.MAX_VALUE
	};

	// console.log('mongo', shell);
	// console.log('mongoose ', mongoose);
	mongoose.Promise = global.Promise;
	mongoose.connect(url, options);

	db.once('open', function() {
	  	console.log('You are connected to mongoDB: ', shell);
	});

	db.on('reconnect', function () {
    	console.log('reconnect...');
	});	

	db.on('close', function () {
    	console.log('Error...close');
	});

	db.on('error', function (err) {
	    console.log('Error...error', err);
	});

	db.on('disconnect', function (err) {
	    console.log('Error...disconnect', err);
	});

	db.on('disconnected', function (err) {
	    console.log('Error...disconnected', err);
	});

	// db.on('parseError', function (err) {
	//     console.log('Error...parse', err);
	// });

	// db.on('timeout', function (err) {
	//     console.log('Error...timeout', err);
	// });


}();

// const Mongo  = {};
const Mongo = {
	save: (model, data) => {
		let promise = new Promise((resolve, reject)=>{
			try{
				var query = new model(data);
			}catch(err){
				reject(err);
			}
			query.save(function (err, data) {
			  	if (err) {
			  		return reject(err);
			  	}
				return resolve(data);
			});
		});

		return promise;
	},

	find: (model, find, filter) => {
		let promise = new Promise((resolve, reject) =>{
			model.find(find, filter, function (err, data) {
			  	if (err) {
			  		return reject(err);
			  	}
			  	return resolve(data);
			});
		});

		return promise;
	},	

	count: (model, find, filter) => {
		let promise = new Promise((resolve, reject) =>{
			model.count(find, function (err, data) {
			  	if (err) {
			  		return reject(err);
			  	}
			  	return resolve(data);
			});
		});

		return promise;
	},

	findOne:(model, filter)=>{
		let promise = new Promise((resolve, reject) =>{
			model.findOne(filter, function (err, data) {
			  	if (err) {
			  		return reject(err);
			  	}
			  	return resolve(data);
			});
		});

		return promise;
	},

	findOneAndUpdate: (model, filter, update, options)=>{
		let promise = new Promise((resolve, reject) =>{
			model.findOneAndUpdate(filter, update, options, 
			(err, data) => {
				if(err){
					return reject(err);
				}
				return resolve(data);
			});
		});

		return promise;
	}
}

export default Mongo;