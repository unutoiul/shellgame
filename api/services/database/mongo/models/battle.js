import mongoose from 'mongoose';

var battleSchema = mongoose.Schema({
    createAt: { type: Date, default: Date.now },
    createBy: String,
    gameId: String,
    gameName: String,
    entry: Number,  
    opponents: Array,
    scheduled: Date
});

var BattleModel = mongoose.model('battles', battleSchema);

export default BattleModel;