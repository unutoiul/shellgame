import mongoose from 'mongoose';

import vars from './../../../../lookups/vars.json';

import { getCurrency, getAmount } from './../../../../utils/bitcoin';

const accountSchema = mongoose.Schema({
    email: { type: String, unique: true, index:true, required: false, sparse:true }, 
	fullName: { type: String, unique: true, required:true },
    password: { type: String, required:false },
    createdAt: { type: Date, required:true },
    faucetAt: { type: Date, required:false },
    wallet: {
    	id: { type: String, required:true },
    	address: { type: String, required:true },
    	currency: { type: String, required:true, get: getCurrency},
    	totalAmount: { type: Number, required:true, get: getAmount },
    }
});

accountSchema.set('toJSON', { getters: true });

const UserModel = mongoose.model('accounts', accountSchema);

export default UserModel;