import mongoose from 'mongoose';
import vars from './../../../../lookups/vars.json';

import { getAmount } from './../../../../utils/bitcoin';

const transactionSchema = mongoose.Schema({
		date: { type: Date },
		transactionId: { type: String },
		currency: { type: String },
		type: {type:String },
		status: {type:String },
		address: {type:String },
		amount: { type: Number, get: getAmount }
});

const transactionsSchema = mongoose.Schema({
    accountId: { type: String, unique: true }, 
    transactions: [transactionSchema]
});

transactionSchema.set('toJSON', { getters: true });

const TransactionsModel = mongoose.model('transactions', transactionsSchema);

export default TransactionsModel;