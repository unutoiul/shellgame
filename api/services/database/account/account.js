import Mongo from './../mongo/mongo_db';

import accountModel from  './../mongo/models/account';
import transactionsModel from  './../mongo/models/transactions';

import transformTransactions from './../../../transformers/services/accounts/transactions';

const Account = {
	create: (user)=>{
		return Mongo.save(accountModel, user);
	},


	find: (id) => {

	},

	findAll: ()=>{

	},

	count: async (id) => {
		let filter = {};
		let data = await Mongo.count(accountModel, filter);
		
		return data;
	},	

	getData: async (id) => {
		let filter = { _id: id };
		let data = await Mongo.findOne(accountModel, filter);
		return data;
	},

	getDataByAddress: async (address) => {
		let filter = { 'wallet.address': address };
		let data = await Mongo.findOne(accountModel, filter);
		return data;
	},

	update: async (id, data) => {
		let filter = { _id: id };
		let options = { new: true };
		let update = await Mongo.findOneAndUpdate(accountModel, filter, data, options);
		return update;
	},

	updateWallet: async (id, amount) => {
		let filter = { _id: id };
		let update = { $inc: { 'wallet.totalAmount': amount }};
		let options = { new: true };
		let updateWallet = await Mongo.findOneAndUpdate(accountModel, filter, update, options);
		return updateWallet;
	},	

	updateFaucet: async (id, time) => {
		let filter = { _id: id };
		let update = { 'faucetAt': time };
		let options = { new: true };
		let updateFaucet = await Mongo.findOneAndUpdate(accountModel, filter, update, options);
		return updateFaucet;
	},
	
	verify: async (email, password) => {
		let filter = { email:email, password:password };
		let data = await Mongo.findOne(accountModel, filter);
		return data;
	},

	nameDuplicated: async (fullName) => {
		let filter = { fullName: fullName };
		let data = await Mongo.findOne(accountModel, filter);
		return data;
	},

	emailDuplicated: async (email) => {
		let filter = { email: email };
		let data = await Mongo.findOne(accountModel, filter);
		return data;
	},

	getTransactions: async (accountId, status) => {
		let find = { accountId: accountId };
		let filter = (status === 'all') ? {} : { transactions: { $elemMatch: { status:{ $in: status }}}};
		let data = await Mongo.find(transactionsModel, find, filter);
		return data;
	},	

	addTransaction: async (accountId, data) => {
		let filter = { accountId: accountId };
		let transaction = transformTransactions(data);
		let options = { upsert: true, new: true };
		let update = await Mongo.findOneAndUpdate(transactionsModel, filter, { $push: { transactions: transaction }}, options);

		return update;
	}

	// findAccountByAddress: (address) => {
	// 	let promise = new Promise((resolve, reject) =>{
	// 		accountModel.findOne({ 'wallet.address': address }, (err, account) => {
	// 			if(err){
	// 				return reject(err);
	// 			}
	// 			return resolve(account);
	// 		});
	// 	});

	// 	return promise;
	// }
}

export default Account;