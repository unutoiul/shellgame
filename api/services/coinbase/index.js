import { Client } from 'coinbase';

import config from './../../config';

const client = new Client({'apiKey': config.coinbase.mykey, 'apiSecret': config.coinbase.mysecret});

const Coinbase = {
	getAccounts: () => {
		let promise = new Promise((resolve, reject)=>{
			console.log('err');
			client.getAccounts({}, function(err, accounts) {
				console.log(err);
				if(err){
					return reject(err);
				}
				return resolve(accounts);
			});
		});

		return promise;
	},

	getAccount: (id, callback) => {
		client.getAccounts(id, function(err, account) {
			callback(err, account);
		})
	},

	createAddress: (id) => {
		let promise = new Promise((resolve, reject)=>{
			client.getAccount(id, function(err, account) {
				if(err){
					return reject(err);
				}

				account.createAddress(null, function(err, address){
					if(err){
						return reject(err);
					}
					return resolve(address);
				});
			});
		});

		return promise;
	},

	getAddresses: (id) => {
		let promise = new Promise((resolve, reject)=>{
			client.getAccount(id, function(err, account) {
				if(err){
					return reject(err);
				}
				account.getAddresses({}, function(err, addresses) {
					if(err){
						return reject(err);
					}
					return resolve(addresses);
			  	});
			});
		});

		return promise;
	},

	getTransactions: (accountId, addressId, callback) => {
		client.getAccount(accountId, function(err, account) {
			account.getAddress(addressId, function(err, address) {
			    console.log(address);
			    address.getTransactions({}, function(err, txs) {
			      	callback(err, txs);
			    });
		  	});
		});
	},
	getNotifications: (callback) => {
		client.getNotifications({}, function(err, notifications) {
			console.log(notifications);
			callback(err, notifications);
		});
	},

	verifyCallback: (payload, signature)=>{
		let verify = client.verifyCallback(JSON.stringify(payload), signature);
		return verify;
	}
}

export default Coinbase;