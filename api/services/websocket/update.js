import db from './../database/db';
import ws from './ws';

import { getSocketId } from './../../utils/string';

const sockets = db.getSocket();
const rooms = db.getRooms();

const Update = {
	sendByAccountId: (accountId, data) => {
		let socket = db.findSocket(accountId);

		if(socket){
			//update wallet
			ws.send(getSocketId(socket), {name: 'updateUser', data: data}, function(err){
				if(err){
                    console.log(err);
					return err;
                }
			});

			let roomIds = db.updateRoomWallet(getSocketId(socket), data);

			for(let i in roomIds){
				let room = db.getRoom([roomIds[i]]);
				let players = room.players;

				//update room if player inside a room
				ws.sendToPlayers(players, {name: 'updateRoom', data: room}, function(err){
	                if(err){
                	 	console.log(err);
	                    return err;
	                }
				});
			}
		}
	},
	homepage: () => {
	    let event = {
	        name: 'online',
	        data: {
	            users: Object.keys(sockets).length,
	            rooms: Object.keys(rooms).length
	        }
	    };

		ws.sendToAll(event);
	}
}

export default Update;
 		
