import db from './../database/db';

const Websocket = {
	sendToPlayers: (players, message, callback) =>{
		for(let i in players){
 			let socket = db.getSocket(i);
 			if(socket){
    			socket.send(message, callback);
 			}
		}
	},

	send: (socketId, message, callback) =>{
		let socket = db.getSocket(socketId);
		if(socket){
			socket.send(message, callback);
		}
	},

	sendToAll: (data) => {
		let sockets = db.getSocket();

	    for (let i in sockets) {
	        sockets[i].send(data , (err) => {
	            if(err){
	                return err;
	            }
	        });
	    }   
	}
}

export default Websocket;