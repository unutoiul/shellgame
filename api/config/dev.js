import fs from 'fs';

var config = {
	env: 'dev',
	port: 8000,
	frontend: './../../../frontend',
	build: '/build',
	libs: '/node_modules',
	partials: '/build/partials',
	index: '/index.html',
	notifications: 'notifications.txt',
	recaptcha: {
		timeout: 15,
		verify: 'https://www.google.com/recaptcha/api/siteverify',
		secretKey: '6LeZACgUAAAAAIGbHUa-a2o4cz_FDZmXSqe_DqDg'
	},
	tls: {
    	key: fs.readFileSync('./data/keys/key.pem', 'utf8'),
    	cert: fs.readFileSync('./data/keys/server.crt', 'utf8')
	},
	email: 'support@shellgame.net',
	smtp:{
		'from':'support@shellgame.net',
		'server':'smtp.shellgame.net',
		'port': '25',
		'secure': false,
		'username': 'support@shellgame.net',
		'password': 'business'
	},
	mongodb:{
		url:'ds157571.mlab.com',
		port :'57571',
		database:'shellgame_dev',
		user:'shellgame',
		password:'business'
	},
	coinbase:{
		mykey: 'sE6CeDNPTOwpGF2p',
		mysecret: 'Fob18cK9eqdevKD5xxasUNXQmXfsaAxV',
		accountID: '62a6cca6-ce7a-5f5e-a749-1c284db74dd3'
	}
};

global.config = config;

module.exports = config;


