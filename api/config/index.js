import env from 'node-env-file';
env(__dirname + './../.env');

var config = require('./' + process.env.NODE_ENV);

global.config = config;

module.exports = config;