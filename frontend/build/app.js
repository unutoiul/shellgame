(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function () {
  'use strict';

	angular.module('app', [
		'ngRoute',
		'ngResource',
		'ngCookies',
		'ngWebSocket', 
		'ngSanitize', 
		'environment',
		'underscore',
		'angularMoment',
		'vcRecaptcha',
		'monospaced.qrcode'
	])

	.config(function($routeProvider, $locationProvider, $httpProvider) {
	    $routeProvider
		    .when('/', { templateUrl: 'partials/main/main.html','access':{'authorization': false}})
		    // .when('/login', { templateUrl: 'partials/login/login.html','access':{'authorization': false}})
		    .when('/play/:id', { templateUrl: 'partials/room/room.html', 'access':{'authorization': false}})
		    .when('/rooms', { templateUrl: 'partials/rooms/rooms.html', 'access':{'authorization': false}})
		    .when('/faq', { templateUrl: 'partials/faq/faq.html','access': {'authorization': true, 'redirect':'login' }})
		    .when('/fairness', { templateUrl: 'partials/fairness/fairness.html','access': {'authorization': true, 'redirect':'login' }})
		    .when('/support', { templateUrl: 'partials/support/support.html','access': {'authorization': true, 'redirect':'login' }})
		    .otherwise({
		    	redirectTo: '/'
		    });

	    // use the HTML5 History API
	    $locationProvider.html5Mode({
	    	enabled: true,
	  		requireBase: false
	    });

	    // Push the interceptor
	    $httpProvider.interceptors.push('interceptor');
	})

	.controller('AppCtrl', function($scope, $location) {
		console.log('$location.path ', $location.path());
		// $scope.setLocation = function(step) {
		// 	$location.path(step);
		// };
	})



	.run(function($rootScope, $location, $templateCache, userData, api) {
		//load template cache
		$templateCache.put('partials/disconnected/disconnected.html', '<div id="disconnected" class="ui modal"><div class="content"><logo type="reverse"></logo><h3>Trying to reconnect...</h3><div class="ui active centered inline loader"></div></div></div>');
		
		//Websockets connection
		$rootScope.$on('$routeChangeStart', function (event, next) {
			// userData.logged(false);
			// console.log('userData', userData.getToken());
			if(userData.getToken()) return;
			
			var redirect = function(){
				// userData.logged(false);
				// $rootScope.logged = false;
				// $rootScope.redirected = true;
				console.log('redirect');
				$location.path(next.access.redirect);
			};

			// if(next.access){
			// 	if(next.access.authorization){
			// 		if(!userData.getToken()){
			// 			redirect();
			// 			return;
			// 		}

			// 		var checkToken = api.checkToken.get();
			// 		checkToken.$promise
			// 		.then(function(data){
			// 			if(data.statusCode === 401){
			// 				redirect();
			// 			}else{
			// 				// userData.userToken = userData.getToken();
			// 				// console.log('userData.getToken ', userData.getToken());
			// 			}
			// 			// else{
			// 				// userData.logged(false);
			// 				// $rootScope.logged = true;
			// 			// }
			// 		})
			// 		.catch(function(event){
			// 			console.log(event);
			// 		});
			// 	}
			// }
		});
	})

	// .controller('AccountCtrl', function($scope, $routeParams) {
	// 	console.log('AccountCtrl');
	//      // $scope.name = "ChapterController";
	//      // $scope.params = $routeParams;
	//  })


	// .controller('DetailsCtrl', function($scope, $routeParams) {
	// 	$scope.details = 'details';
	// 	console.log('DetailsCtrl');
	//      // $scope.name = "ChapterController";
	//      // $scope.params = $routeParams;
	//  })

	// .controller('LoginCtrl', function($scope, $routeParams) {
		// console.log('LoginCtrl');
	     // $scope.name = "ChapterController";
	     // $scope.params = $routeParams;
	 // })

	;

}());


},{}]},{},[1])