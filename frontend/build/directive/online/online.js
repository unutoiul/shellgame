(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')
.directive('online', function () {
  	return {
	    templateUrl: 'assets/directive/online/online.html',
	    // restrict: 'A',
	    controller: 'OnlineCtrl'
  	};
})

.controller('OnlineCtrl', function($scope, userData, api) {
	$scope.$on('online', function(event, data){
		$scope.$evalAsync(function(){
			$scope.roomsOnline = data.rooms;
			$scope.usersOnline = data.users;
		});
	});
})

;
},{}]},{},[1])