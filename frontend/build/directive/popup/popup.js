(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')
.directive('popup', function () {
  	return {
	    templateUrl: 'assets/directive/popup/popup.html',
	    controller: 'PopupCtrl'
  	};
})

.controller('PopupCtrl', function($scope, $element, userData, broadcast, vars, events) {
    var pop = $element.find('.ui.modal');


    $scope.$on(events.popup, function(event, data){
        // console.log('data', data);
        if(data.close){
            var pop = $element.find('.ui.modal');
            pop.modal('hide');
            return;
        }

        $scope.$evalAsync(function(){
           $scope.popup = data.popup;
            $scope.tab = data.tab;
            $scope.closable = data.closable;
        });
 
    });

    $scope.openPopup = function() {
        var pop = $element.find('.ui.modal');

        pop
        .modal({
            detachable: false,
            autofocus: true,
            observeChanges: true,
            closable:  $scope.closable,
            onShow: function(event){
                var tab = angular.element(this).find("[data-tab='" + $scope.tab + "']");
                tab.addClass('active');
            },
            onHidden: function(){
                $scope.$evalAsync(function(){
                    $scope.popup = null;
                });
            }
        })
        .modal('show');
    };


    //init
    if(!userData.connected()){
        broadcast.event(events.popup, {popup: 'startup', tab: 'welcome', closable: false});
    }
})

;
},{}]},{},[1])