(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.directive('chat', function () {
  	return {
	    templateUrl: 'assets/directive/chat/chat.html',
	    controller: 'ChatCtrl'
	    // restrict: 'A'
  	};
})

.controller('ChatCtrl', function($scope, $element, $timeout, api, vars, events) {
	$scope.data = [];
	$scope.chatOpen = false;

 	api.subscribe('/mainChat', updateChat, function(err){
 		if(err){
 			throw new Error(err);
 		}
 	});

 	$scope.$on(events.login, function(event, data){
 		if(!data.connected) return;

       	api.getChatMessages(function(messages){
			$scope.$evalAsync(function(){
				$scope.data = messages;
		    });

		    // console.log('userLogin ', messages);

		    $timeout(function(){
		    	animateScroll();
			});
       	});
    });

	$scope.submit = function(newMessage){
	 	if (!newMessage) { return; }
	 	
	 	$scope.newMessage = '';

 		var message = {
 			message: newMessage,
 			time: new Date()
 		};

 		api.sendChatMessage(message, function(data){
 			// console.log('sendChatMessage', data);
 		});

		// $scope.subscriptions = websocket.subscriptions();
	};

	$scope.chatToogle = function(){
		$scope.chatOpen = !$scope.chatOpen;
		
		// var left = ($scope.chatOpen)? '-300px':'0px';
		// var marginLeft = ($scope.chatOpen)? '0px':'300px';
		// var ease = ($scope.chatOpen)? 'easeOut':'easeIn';
		// $element.find('#chat').animate({'left': left}, 200);
		// angular.element('#view').animate({'marginLeft': marginLeft}, 200);
	};

	function updateChat(data, flags){
		$scope.$evalAsync(function(){
		 	$scope.data.push(data);
	    });

		animateScroll();
	}

	function animateScroll() {
		var messages = $element.find('.messages');
		messages.animate({ scrollTop: messages.prop("scrollHeight")}, 500);
	}
})

;
},{}]},{},[1])