(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.directive('limits', function () {
  	return {
	    templateUrl: 'assets/directive/limits/limits.html',
	    // restrict: 'A',
	    // controller: 'betAmountCtrl'
  	};
})

.controller('limitsCtrl', function ($scope, $element, $location, api) {
	// console.log('limitsCtrl');

	$scope.slider = {
		minValue:1,
 	 	options: {
 	 		showTicks: true,
 	 		hidePointerLabels: true,
		    hideLimitLabels: true,
		    // autoHideLimitLabels: true,
 	 		stepsArray: [
				{value: 'Ƀ 0.001 / Ƀ 0.01', legend: 'Low Roller'},
		      	{value: 'Ƀ 0.01 / Ƀ 0.1', legend: 'Medium Roller'},
		      	{value: 'Ƀ 0.1 / Ƀ 1', legend: 'High Roller'}
 	 		],

    		// showTicksValues: true,
    		showSelectionBar: true,
		  	translate: function(value){
		  		$scope.sliderValue = value;
		  	}
	  	}
	};


	$scope.findARoom = function(){
		//hide popup
		var pop = $element;
		pop.modal('hide');

		//create a rooom
		var data = {
			limits: $scope.sliderValue,
	    	players: {},
	    	games: {}
	    };

		api.findARoom(data, function(data){  	
			// console.log('room', data);	
	    	$scope.$apply(function(){
	    		$location.path('/play/'+data.id);
	    	});
		});
	};
})

;
},{}]},{},[1])