(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

angular.module('app')
.directive('loginBox', function () {
  	return {
	    templateUrl: 'assets/directive/login-box/login-box.html',
        controller: 'LoginBoxCtrl'
  	};
})

.controller('LoginBoxCtrl', function($scope, $element, broadcast,  userData, api, regex, vars, events) {
    $scope.regex = regex;

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

 	$scope.login = function(user, valid){
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }
        disableBtn(true);

 		api.login.post({
            'email': user.email,
            'password': user.password
        })
        .$promise
        .then(function(data) {
            disableBtn(false);

            if(data.error){
                message.show();
                $scope.$evalAsync(function(){
                    $scope.error = data.message;
                });
            }

            if(data.token){
                userData.setToken(data.token);
                broadcast.event(events.login, {login:true});
            }
        })
        .catch(function(event){
            disableBtn(false);
            $scope.$evalAsync(function(){
                $scope.error = event.data;
            });
        });
    };
});
},{}]},{},[1])