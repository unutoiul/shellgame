(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')
.directive('balance', function () {
  	return {
	    templateUrl: 'assets/directive/balance/balance.html',
        controller: 'BalanceCtrl'
	    // restrict: 'A',
  	};
})

.controller('BalanceCtrl', function($scope, $element, broadcast, userData, api, vars, events) {
	$element.find('.dropdown').dropdown();

    $scope.$on(events.login, function(event, data){
        if(data.connected){
            api.getAccount(function(data){
                broadcast.event(events.updateUser, data);
                userData.setData(data);
            });
        }
    });

    $scope.$on(events.updateUser, function(event, data){
        updateData(data);
        startAni();
    });

    $scope.getToken = function() {
        return userData.getToken();
    };

    $scope.loadPopup = function(popup, tab) {
        broadcast.event(events.popup, {popup: popup, tab:tab});
    };

	$scope.logout = function() {
        api.disconnect(function(){
            userData.removeToken();
            broadcast.event(events.login, { connected:false });
            broadcast.event(events.popup, { popup: 'startup', tab:'login', closable: false});
        });
	};

    function startAni(){
        var totalAmount = $element.find('.totalAmount');
        var sec = 0.2;

        TweenLite.to(totalAmount, sec, {scale:1.1});
        TweenLite.to(totalAmount, sec, {scale:1, delay: sec});
    }
    function updateData(data){
        $scope.$evalAsync(function() {
            $scope.userData = data;
        });

        userData.setData(data);
    }
});
},{}]},{},[1])