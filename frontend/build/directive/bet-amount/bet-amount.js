(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.directive('betAmount', function () {
  	return {
	    templateUrl: 'assets/directive/bet-amount/bet-amount.html',
	    link: function(scope, element, attrs){
	    	
	    }
	};
})

.controller('betAmountCtrl', function ($scope, $element, $attrs, api, regex) {
	var vm = this;
	vm.betAmount = '0.001';

	$element.find(".betValues li").click(function($event){
		var value = angular.element($event.target).data("value");
		var type = angular.element($event.target).data("type");

		$scope.$evalAsync(function(){
			vm.betAmount = (type === 'operator') ? vm.betAmount * value : value;
		});
	});

	$scope.regex = regex;

	$scope.$on('setBet', function(event, arg_bet){
		$scope.$apply(function(){
			vm.betAmount = arg_bet;
		});
	});

	$scope.$on('betAmount', function(event, arg_b){
		var tabs = $element.find('.betValues li');
		var input = $element.find('.betAmount input');
		var label = $element.find('.betAmount .label');
		var sec = 200;
		
		var marginRadius = (arg_b) ? '0.2em' : '0';


		if(arg_b){
			tabs
	 		.css({zIndex: '-1'})
	 		.animate({
	    		marginTop: '-40px',
		  	}, sec, function(event){
		  		angular.element(this).css({display:  'none'});
		  	});
		}else{
			tabs
	 		.css({display: 'block'})
	 		.animate({
	    		marginTop: '0px',
		  	}, sec, function(event){
		  		angular.element(this).css({zIndex: '1'});
		  	});
		}


		input.css({borderBottomRightRadius: marginRadius});
		label.css({borderBottomLeftRadius: marginRadius});

		if(arg_b){
			label.addClass('disabled');
			input.attr('disabled', true);
		}else{
			input.removeAttr('disabled');
			label.removeClass('disabled');
		}
	});
})

;
},{}]},{},[1])