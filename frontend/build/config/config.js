(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')
.config(function(envServiceProvider) {
	envServiceProvider.config({
		domains: {
				development: ['localhost'],
				production: ['shellgame.net'],
		},
		vars: {
			development: {
				ws: 'ws://',
				http: 'http://',
				domain: 'localhost',
				port: ':8000',
				api: '/api',
				disconnected: 5,
				recaptcha: {
					siteKey: '6LeZACgUAAAAAPye-jM_0BhLVk2qsei98-jnEf6z',
				}
			},
			production: {
				ws: 'wss://',
				http: 'https://',	
				domain: 'shellgame.net',
				port: '',
				api: '/api',
				disconnected: 30,
				recaptcha: {
					siteKey: '6LfuEygUAAAAAPnaN6pAq4ywWtngESIpzZgbHk2U',
				}
			}
		},
	});

	envServiceProvider.check();
});
},{}]},{},[1])