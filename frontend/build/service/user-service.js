(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.service('userData', function($cookies) {
    var token = null;
    var socketId = null;
    var data = null;
    var connected = false;
    // var online = null;

    return {
        setConnection: function(b){
            connected = b;
        },

        connected: function(){
            return connected;
        },

        setToken: function(arg_token) {
            token = arg_token;
            $cookies.put('x-token', arg_token);
            
            return token;
        },
        
        getToken: function() {
            token = (token) ? token : $cookies.get('x-token');
            if (!token) {
                return null;
            }
            return token;
        },

        removeToken: function(){
            token = null;
            $cookies.remove('x-token');
        },

        setSocketId: function(arg_socket_id){
            socketId = arg_socket_id;
        },

        socketId: function() {
            return socketId;
        },

        setData: function(arg_data){
            data = arg_data;
        },
        
        getData: function(){
            return data;
        }

        // updateOnline: function(arg_online){
            // online = arg_online;

            // console.log('updateOnline');
            // return online;
            // console.log('updateOnlineUsers', arg_online)
            // $rootScope.$emit('updateOnlineUsers', arg_online);
        // }, 

        // getOnline: online;
    };
});

},{}]},{},[1])