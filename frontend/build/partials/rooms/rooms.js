(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('RoomsCtrl',function($scope, $location, userData, api) {

	$scope.$on('update', function(event, data){
		if(!data.online){
			$scope.$apply(function(){
				$scope.rooms = data;
			});
		}
	});

	// api.getRooms(function(data){
	// 	$scope.$apply(function(){
	// 		$scope.rooms = data;
	// 	});
	// });

	$scope.createGame = function(){
		//create a rooom
		var data = {
	    	amount: Math.random(),
	    	players: {},
	    	games: {}
	    };

		api.createRoom(data, function(data){     		
	    	$scope.$apply(function(){
	    		$location.path('/play/'+data.id);
	    	});
		});
	};

	$scope.join = function(roomId){
		//join a rooom
		api.joinRoom(roomId, function(err, data){
			if(err){
				$scope.$apply(function(){
					$scope.error = err;
	    		});
	    		return err;	
			}

    		$scope.$apply(function(){
	    		$location.path('/play/'+roomId);
	    	});
		});
	};
});

},{}]},{},[1])