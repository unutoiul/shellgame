(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('UpdateAccountCtrl', function($scope, $element, broadcast,  userData, api, regex, vars, events) {
    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    $scope.emailPattern = regex.email;

    $scope.update = function(user, valid){
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }

        disableBtn(true);
        api.updateAccount(user, function(err, data){
            if(err){
                $scope.$evalAsync(function(){
                    $scope.message = err.message;
                });
                message.show();
                disableBtn(false);
                return;
            }

            broadcast.event(events.popup, {close:true});
            
            disableBtn(false);
        });
    };

    $scope.close = function(){
        broadcast.event(events.popup, {close: true});
    };
});
},{}]},{},[1])