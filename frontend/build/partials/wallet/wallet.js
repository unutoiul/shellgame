(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('WalletCtrl', function($scope, $element, $timeout, moment, events, broadcast, userData, envService, api, regex, vars) {
   	// init
    var config = envService.read();

    var btn = $element.find('.copyAction');
   	var popup = $element.find('.custom.popup');
   	var address = $element.find('.bitcoinAddress');

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    $element.find('.top.menu .item').tab();

    $scope.siteKey = config.recaptcha.siteKey;
    $scope.regex = regex;

    $scope.$evalAsync(function() {
        $scope.withdrawSubmitted = false;
        $scope.faucetSubmmited = false;
        $scope.faucetStatus = false;
    });

    $scope.back = function(){
        $scope.$evalAsync(function() {
            $scope.withdrawSubmitted = false;
        });
    };

   	angular.element(btn)
	.popup({
    	on: 'click',
    	variation: 'inverted',
    	target: address,
    	popup: popup,
    	position: 'bottom center',
    	onShow: function($event){
			address.select();
			document.execCommand('copy');

			$timeout(function(){
				angular.element($event).popup('hide');
			}, 800);
	    }
  	});


    //Submit Withdraw
	$scope.submitWithdraw = function(data, valid) {
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }

        disableBtn(true);
        data.type = 'withdraw';
        api.addTransaction(data, function(err, data){
            disableBtn(false);

            if(err){
                $scope.$evalAsync(function(){
                    $scope.message = err.message;
                });
                $element.find('.message').show();
            }else{
                $scope.$evalAsync(function() {
                    $scope.withdrawSubmitted = true;
                });

            }
        });
    };

    //Claim Faucet
    $scope.submitFaucet = function(captcha){
        if(!captcha){
            $scope.$evalAsync(function(){
                $scope.message = 'Please select the tickbox to proof you are not a robot.';
            });
            $element.find('.message').show();
            return;
        }

        $element.find('.message').hide();

        api.faucet(captcha, function(data){
            if(!data.success){
                $scope.$evalAsync(function(){
                    $scope.message = 'Bad request was made. Please try again.';
                });
                $element.find('.message').show();
            }

            $scope.$evalAsync(function() {
                $scope.faucetStatus = true;
                $scope.faucetMessage = 'Succefully claim faucet.';
            });

            $timeout(function(){
                broadcast.event(events.popup, {close:true});
            }, 500);
        });
    };

    if(userData.getData().wallet.totalAmount > 0){
        $scope.$evalAsync(function() {
            $scope.faucetStatus = true;
            $scope.faucetMessage = 'You need to spend all of your bitcoins before can claim again.';
        });
    }  
    
    var faucetAt = userData.getData().faucetAt;


    if(faucetAt){
        // console.log(moment(faucetAt).format("HH:mm:ss"));
        // console.log(moment().format("HH:mm:ss"));
        // console.log(moment(faucetAt) > moment());

        if(moment(faucetAt) > moment()){
            var diff = moment(faucetAt).diff(moment(),'m');

            $scope.$evalAsync(function() {
                $scope.faucetStatus = true;
                $scope.faucetMessage = 'You might need to wait <b>'+diff+' minutes</b> before you can claim again.';
            });
        }
        
    }
});
},{}]},{},[1])