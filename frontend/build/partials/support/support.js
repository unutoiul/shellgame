(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('SupportCtrl', function ($scope, $element, api) {

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    $scope.$evalAsync(function(){
    	$scope.supportView = 'send';

    });


	$scope.sendMessage = function(message, valid){
		if(!valid) return;

		disableBtn(true);

		api.sendMessage(message, function(err, data){
			disableBtn(false);
			if(err){
				$scope.$evalAsync(function(){
					$scope.error = err;
				});
			}

		  	$scope.$evalAsync(function(){
		    	$scope.supportView = 'successful';
	    	});


			console.log('data ', data);
		});
	};
})

;
},{}]},{},[1])