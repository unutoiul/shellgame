(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('StartupCtrl', function($rootScope, $scope, $element, broadcast, userData, api, vars, events) {
    $scope.box = 'loading';

    $scope.$on(events.login, function(event, data){
        if(data.login){
            $scope.startConnect(data);
        }
    });
    
    $scope.showBox = function(string){
        $scope.$evalAsync(function(){
            $scope.box = string;
        });
    };

    $scope.startConnect = function(arg_data){
        if(userData.getToken()){
            var auth = { auth:{ headers:{ authorization:  userData.getToken() }}};
            api.connect(auth, function(data){
                if(data.socketId){
                    if(arg_data.updateAccount){
                        broadcast.event(events.popup, {popup: 'update-account', tab: null, closable: true});
                    }

                    // if(arg_data.login){
                        // broadcast.event(events.popup, {close:true});
                    // }
                }else{
                    userData.removeToken();
                    $scope.showBox('login');
                }
            });
        }else{
            $scope.showBox($scope.tab);
        }
    };


})

;
},{}]},{},[1])