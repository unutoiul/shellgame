(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')

.controller('RoomCtrl',function($scope, $compile, $routeParams, $location, $element, $timeout, envService, broadcast, counter, userData, websocket, api, vars, events) {
	var config = envService.read();
	var roomId = $routeParams.id;
	var showBetAmount = [24,25,26,27,28];
	var messageUI = $element.find('.message.error');

	//ini cups
	$scope.cups = [{ id: 'a'},{ id: 'b' }];

	$scope.timeout = false;
	$scope.timeoutStart = config.disconnected;
	
 	//initialize room
 	if(userData.connected()){
		getRoom();
 	}

	//Event listeners
	$scope.$on('updateRoom', function(event, data){
		console.log('userData.socketId() ', userData.socketId());
		console.log('updateRoom ', data);
		var status = data.players[userData.socketId()].status;
		updateRoom(data);
		setStatus(data, status);
	});

 	$scope.$on(events.login, function(event, data){
        if(data.connected){
		 	getRoom();
        }
    });

	$element.on('$destroy', function() {
       counter.stop();
  	});

	$scope.$on('$locationChangeStart', function(event) {
		// console.log('locationChangeStart');
	    // var confirmation = confirm('are you sure you wanna leave this room?');

	    // if(confirmation){
		 	leaveRoom();
	    // }else{
			// event.preventDefault();
	    // }
	});
 	
	//Set init bet
 	$scope.selectCup = function($event, cupId){
 		if($scope.cupId === cupId){
 			angular.element($element).find('.cupWrapper').removeClass('active');
 			disableButton(true);
 			$scope.cupId = null;
 			return;
 		}

 		angular.element($element).find('.cupWrapper').removeClass('active');
	 	angular.element($event.currentTarget).addClass('active');

 		$scope.cupId = cupId;

 		disableButton(false);
 	};

	function getRoom(){
		$scope.socketId = userData.socketId();
		
 		api.getRoom(roomId, function(room){

			if(!room){
				return $location.path('/');
			}
		});	
 	}

 	//set status of the room
	function setStatus(room, status){
		var animate = [25, 26, 27, 28];

		if(animate.indexOf(status.code) != -1){
			animateCup(status.code, room.game);
		}

		$scope.$evalAsync(function(){
			$scope.status = status;
			$scope.showBetAmount = (showBetAmount.indexOf(status.code) > -1);
		});

		messageUI.hide();

		// console.log('setStatus', status, room);

		switch(status.code){
			case 11:
				//player:connected
				counter.stop();
				$scope.$evalAsync(function(){
					$scope.timeout = false;
				});
			break;

			case 12:
				//player:disconnect
				counter.start(countDisconnect);
				$scope.$evalAsync(function(){
					$scope.timeout = true;
				});
				disableGame(true);
				disableBetAmount(true);
				disableButton(true);
			break;

			case 10:
				//player:waiting
				$scope.$evalAsync(function(){
					$scope.timeout = false;
				});
				disableGame(true);
				disableBetAmount(true);
				disableButton(true);
			break;

			case 22:
				clearGame();
				disableGame(true);
				disableBetAmount(true);
				disableButton(true);
			break;

			case 23:
			case 25:
			case 26:
			case 27:
			case 28:
				disableGame(true);
				disableBetAmount(true);
				disableButton(true);
			break;

			case 24:
				//host set game
				disableGame(false);
				disableBetAmount(true);
				setBetAmount(room.game.betAmount);
			break;

			case 21:
				clearGame();
				disableGame(false);
				disableBetAmount(false);
				
			break;
		}
	}

	function countDisconnect(arg_count){
		var count = config.disconnected - arg_count;
		var timer = $element.find('.messageDisplay .timer');

		timer.text(count);
        if(count === 0){
            counter.stop();
            disconnected();
        }
	}

 	//Play the game
 	$scope.play = function($event, valid){
		var host = $scope.room.players[userData.socketId()].host;
		var button = angular.element($event.target);
		var betAmount = $element.find('.betAmount input').val();

		if(!valid){
			return;
		}

		for(var i in $scope.room.players){
			var player = $scope.room.players[i];

			if(betAmount > player.totalAmount){
				var message = (userData.socketId() === i) ? 
					'You don\'t enough bitcoins in your account. <a class="tealLink" href="#" ng-click=\'loadPopup("wallet","deposit")\'>deposit now</a>' : 
					$scope.room.players[i].name + ' doesn\'t have enough bitcoin in his account. Please make a smaller bet.';

				messageUI.show();
				$scope.error = message;
				return;
			}
		}

		messageUI.hide();
		disableBtn($event.target, true);

 		api.gameAction(roomId, {
 			cupId: $scope.cupId,
 			betAmount: betAmount
 		}, function(err,data){
 			if(err){
 				$scope.$evalAsync(function(){
 					messageUI.show();
 					$scope.error = err;
 				});
 			}
 			disableBtn($event.target, false);
 		});
 	};

   	$scope.loadPopup = function(popup, tab) {
        broadcast.event(events.popup, {popup: popup, tab:tab});
    };

    $scope.swapPlayers = function ($event) {
    	disableBtn($event.target, true);

    	api.swapPlayers(roomId, function(err, data){
    		if(err){
    			console.log(err);
    		}
    		console.log('data ', data);
    		disableBtn($event.target, false);
    		
    	});
    };

 	function animateCup(statusCode, game){
 		var cupId = game.cupSelected;
 		var li = $element.find('.game #' + cupId);
 		var cup = li.find('.cup');
 		var bitcoin = li.find('.bitcoin');
 		var moveX = (cupId === 'a')? '-30px' : '30px';
 		var rotate = (cupId === 'a')? '-15' : '15';
 		var delay = (statusCode === 24) ? 0 : 0.6;
 		var bitcoinCodes = [25, 28];
 		var bitcoinShow = (bitcoinCodes.indexOf(statusCode) != -1) ? true : false;

		//open

		TweenLite.to(cup, 0.3, {y:'-80px', x: moveX, rotation: rotate});
		
		if(bitcoinShow){
			TweenLite.fromTo(bitcoin, 0.1, { opacity:0 }, { opacity: 1, display:'block', delay: 0 });
		}

		// if(statusCode != 24){
		// 	TweenLite.to(cup, 0.5, {y:'0', x:'0', rotation: 0, delay: delay});
		// }
 	}

    function disableBtn(target, arg_b){
        if(arg_b){
            target.addClass('disabled loading');
        }else{
            target.removeClass('disabled loading');
        }
    }

	function updateRoom(room, flags){
		$scope.$evalAsync(function(){
			$scope.room = room;
		});
	}

 	function clearGame(){
 		var li = angular.element($element).find('.game li');
 		var cup = li.find('.cup');
 		var bitcoin = li.find('.bitcoin'); 

 		$scope.$evalAsync(function(){
 			$scope.cupId = null;
 		});
		li.removeClass('active');
		TweenLite.to(cup, 0.5, {y:'0', x:'0', rotation: 0});
		TweenLite.to(bitcoin, 0, {opacity:0});
 	}

	function disableGame(bool){
		// console.log('disableGame' , bool);

		$scope.$evalAsync(function(){
			$scope.disableGame = bool;
		});
	}	

	function disableButton(bool){
		// console.log('disableButton' , bool);

		$scope.$evalAsync(function(){
			$scope.disableButton = bool;
		});
	}

	function setBetAmount(bet){
		broadcast.event('setBet', bet);
	}

	function disableBetAmount(bool){
		broadcast.event('betAmount', bool);
	}
	
	function leaveRoom(){
    	api.leaveRoom(roomId, function(err, room){
    		// console.log('leaveRoom ',err, room);
    	});
	}

	function disconnected(){
		api.disconnected(roomId, function(err, room){
    		// console.log('disconnected ',err, room);
    	});
	}

	// function errorHandler(err){
	// 	if(err){
 	//           throw new Error(err);
 	//        }
	// }

 	// $scope.errorHandler = function(err){
 	// 	console.log(err);
 	// };
});
},{}]},{},[1])