(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module('app')
.constant('vars', {
	gameStatus: {
		'player:waiting': 'Waiting for player to join...',
		'player:connected': 'Please select a cup.'  ,
		'player:disconnect': 'Waiting for player to reconnect.' ,
		'player:disconnected': '[Player Name] was disconnected.',
		'game:start': '[Player Name] is selecting.',
		'game:end': '[Player Name] is won 1 btc.',
	},
	errors:{
		'form': 'There is an error validating the form.'
	}

});
},{}]},{},[1])