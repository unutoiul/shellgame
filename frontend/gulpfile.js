var config = require('./config.json');
var gulp = require('gulp');
var del = require('del');
var concat = require('gulp-concat');
var using = require('gulp-using');
var uglify = require('gulp-uglify');
var rimraf = require('rimraf');
var ngAnnotate = require('gulp-ng-annotate');
var lessImport = require('gulp-less-import');
var addsrc = require('gulp-add-src');
var jshint = require('gulp-jshint');
var inject = require('gulp-inject');
var print = require('gulp-print');
var less = require('gulp-less');
var browserify = require('gulp-browserify');
var es = require('event-stream');

// var host = 'http://localhost:8000/';
// var lessFiles = gulp.src(config.path.less.src)
//     .pipe(concat('lucas.css'))
//     .pipe(less())
//     .pipe(gulp.dest(config.bases.build + 'css'));

var swallowError = function(err) {
    console.log(err);
    this.emit('end');
};

gulp.task('clean', function(cb) {
     rimraf(config.bases.build, cb);
    // gulp.src(config.bases.build, { read: false })
    // .pipe(clean({ force: true }));
});

gulp.task('build', ['html_build', 'js_build', 'css_build', 'less_build', 'images_build', 'fonts_build', 'bootfile_build']);

gulp.task('scripts_bin', ['clean', 'jshint'] ,function() {
  // Minify and copy all JavaScript (with vendor scripts)
  return gulp.src(config.path.scripts.all)
    // ng annotate project js files
    .pipe(ngAnnotate())
    // Pipe libraries js files
    .pipe(addsrc.prepend(config.path.libs))
    // uglify and concat in one single file
    .pipe(uglify())
    .pipe(concat('app.js'))
    // Put in dist/scripts folder
    .pipe(gulp.dest(config.bases.build + 'scripts/'));
});

gulp.task('jshint', function() {
    return gulp
        .src(config.path.scripts.all)
        .pipe(jshint(config.jshint))
        .pipe(jshint.reporter('default'));
});

// Copy all static files
gulp.task('fonts_build', function() {
  return gulp.src(config.path.fonts)
    .pipe(gulp.dest(config.bases.build + 'fonts'));
});

gulp.task('images_build', ['single-files'], function() {
  return gulp.src(config.path.img)
    // Pass in options to the task
    // .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest(config.bases.build + 'img'));
});

//Inject javascript & css files
gulp.task('bootfile_build', function() {
    var injects = [
        { name: 'jslibs', addPrefix: 'libs', src: config.path.libs, ignorePath: 'node_modules' }, 
        { name: 'csslibs', addPrefix: 'libs', src: config.path.css.libs, ignorePath: 'node_modules' }, 
        { name: 'css', addPrefix: 'assets', src: config.path.css.build, ignorePath: 'build' }, 
        { name: 'boot', addPrefix: 'assets', src: config.path.scripts.boot, ignorePath: 'src' }, 
        { name: 'configuration', addPrefix: 'assets', src: config.path.scripts.config, ignorePath: 'src' }, 
        { name: 'services', addPrefix: 'assets', src: config.path.scripts.services, ignorePath: 'src' }, 
        { name: 'factories', addPrefix: 'assets', src: config.path.scripts.factories, ignorePath: 'src' }, 
        { name: 'directives', addPrefix: 'assets', src: config.path.scripts.directives, ignorePath: 'src' }, 
        { name: 'utils', addPrefix: 'assets', src: config.path.scripts.utils, ignorePath: 'src' }, 
        { name: 'filters', addPrefix: 'assets', src: config.path.scripts.filters, ignorePath: 'src' }, 
        { name: 'lookups', addPrefix: 'assets', src: config.path.scripts.lookups, ignorePath: 'src' }, 
        { name: 'partials', addPrefix: 'assets', src: config.path.scripts.partials, ignorePath: 'src' },
    ];

    var stream = gulp.src(config.path.bootFile);
    
    //Inject scripts
    for(var i in injects){
        var source = gulp.src(injects[i].src, {read: false}).pipe(using({}));
        stream = stream.pipe(inject(source, {relative: false, name:injects[i].name, addPrefix: injects[i].addPrefix, ignorePath: injects[i].ignorePath}))
    }

    stream.pipe(gulp.dest(config.bases.build));

    return stream;
});

// Copy all static html
gulp.task('html_build',  function() {
    return gulp.src(config.path.html)
        .on('error', swallowError)
        .pipe(gulp.dest(config.bases.build))
});

gulp.task('js_build', ['jshint'] ,function() {
    gulp.src(config.path.scripts.all)
        .pipe(browserify())
        .on('error', swallowError)
        .pipe(gulp.dest(config.bases.build));
});

gulp.task('less_build', function() {
    return gulp.src(config.path.less.src)
        .pipe(lessImport(config.path.less.build))
        .pipe(less())
        .on('error', swallowError)
        .pipe(gulp.dest(config.bases.build + 'css'));
});

gulp.task('css_build', function() {
	return gulp.src(config.path.css.src).pipe(gulp.dest(config.bases.build + 'css'));
});

gulp.task('single-files', function() {
    gulp.src(config.path.singlefiles, {base: config.bases.src})
        .pipe(gulp.dest(config.bases.build));
});

gulp.task('watch', ['build'], function(){
    gulp.watch(config.path.bootFile, ['bootfile_build']);
    gulp.watch(config.path.html, ['html_build']);
    // gulp.watch(config.path.scripts, ['ts_build']);
    gulp.watch(config.path.scripts.all, ['js_build']);
    gulp.watch(config.path.less.src, ['less_build']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['bin']);