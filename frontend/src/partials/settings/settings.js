angular.module('app')

.controller('SettingsCtrl', function ($scope, $element, api, regex, vars) {

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

	$scope.regex = regex;

	api.getAccount(function(data){
        $scope.$evalAsync(function() {
            $scope.userData = data;
        });
    });

    $scope.user = {
    	fullName: $scope.userData.fullName,
    	email: $scope.userData.email
    };

	$scope.update = function(user, valid){
		console.log('updateAccount ', user);
 		var message = $element.find('.message').show();

	 	if (!valid) { 
        	message.removeClass('success').addClass('error');
        	$scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }

		user.update = ($scope.userData.email) ? false : true;

		disableBtn(true);

		api.updateAccount(user, function(err, data){
            if(err){
                $scope.$evalAsync(function(){
                    $scope.message = err.message;
                });
                message.removeClass('success').addClass('error');

                disableBtn(false);
                return;
            }

            $scope.$evalAsync(function(){
        	 	$scope.userData = data.data;
           		$scope.message = data.message;
           	});

           	message.removeClass('error').addClass('success');

            console.log(data);

            disableBtn(false);
        });
	};
})

;