angular.module('app')

.controller('AccountCtrl', function ($scope, api) {
	var account = api.account.get();

	account.$promise
	.then(function(data){
		$scope.data = data;
	})
	.catch(function(event){
		console.log(event);
	});
})

;