angular.module('app')

.controller('WelcomeCtrl', function($scope, $element, broadcast, userData, api, vars, events) {

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');

        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    disableBtn(true);

    api.countAccounts.get()
        .$promise
        .then(function(data) {
            if(data.error){
                $scope.message = data.message;
                message.show();
            }

            $scope.$evalAsync(function(){
                $scope.countAccounts = data.count;
            });

            disableBtn(false);
            
        })
        .catch(function(event){
            disableBtn(false);

            $scope.message = event.data;
        });

    $scope.join = function(user, valid){
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }
        disableBtn(true);

        api.join.post(user)
        .$promise
        .then(function(data) {
            if(data.error){
                $scope.message = data.message;
                message.show();
            }

            if(data.token){
                userData.setToken(data.token);
                broadcast.event(events.login, {login:true, updateAccount: true});

            }

            disableBtn(false);
        })
        .catch(function(event){
            disableBtn(false);
            $scope.message = event.data.message;
        });
    };


});