angular.module('app')

.controller('LoginCtrl', function($scope, $element, broadcast,  userData, api, regex, vars, events) {
    $scope.regex = regex;

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

 	$scope.login = function(user, valid){
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }
        disableBtn(true);

 		api.login.post({
            'email': user.email,
            'password': user.password
        })
        .$promise
        .then(function(data) {
            disableBtn(false);

            if(data.error){
                message.show();
                $scope.$evalAsync(function(){
                    $scope.error = data.message;
                });
            }

            if(data.token){
                userData.setToken(data.token);
                broadcast.event(events.login, {login:true});
            }
        })
        .catch(function(event){
            disableBtn(false);
            $scope.$evalAsync(function(){
                $scope.error = event.data;
            });
        });
    };
});