angular.module('app')

.controller('UpdateAccountCtrl', function($scope, $element, broadcast,  userData, api, regex, vars, events) {
    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    $scope.emailPattern = regex.email;

    $scope.update = function(user, valid){
        var message = $element.find('.message');
        if (!valid) { 
            message.show();
            $scope.$evalAsync(function(){
                $scope.message = vars.errors.form;
            });
            return;
        }

        disableBtn(true);
        api.updateAccount(user, function(err, data){
            if(err){
                $scope.$evalAsync(function(){
                    $scope.message = err.message;
                });
                message.show();
                disableBtn(false);
                return;
            }

            broadcast.event(events.popup, {close:true});
            
            disableBtn(false);
        });
    };

    $scope.close = function(){
        broadcast.event(events.popup, {close: true});
    };
});