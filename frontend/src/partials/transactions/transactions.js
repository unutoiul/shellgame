angular.module('app')

.controller('TransactionsCtrl', function($scope, $element, $timeout, userData, api) {

	$scope.loader = true;

	$scope.getTransactions = function($event, status){
    	if($event){
	    	angular.element($event.target).siblings().removeClass('active');
	    	angular.element($event.target).addClass('active');
    	}

   		$scope.$evalAsync(function() {
    		$scope.loader = true;
		});

		//Get transactions
	   	api.getTransactions(status, function(data){
	   		console.log('transactions ' , data);

	   		$scope.$evalAsync(function() {
				$scope.data = data[0];
	   			$scope.loader = false;
	   		});
	   	});
	};
});