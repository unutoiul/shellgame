angular.module('app')

.controller('MainCtrl', function ($scope, $element, $timeout, $location, userData, api, broadcast, events, _) {

    // init
    $scope.cups = [{ id: 'a'},{ id: 'b' }];

    $scope.openPopup = function(popup, tab, closable){
        broadcast.event(events.popup, {popup: popup, tab: tab, closable: closable});
    };

    $scope.findARoom = function($event){
        angular.element($event.target).addClass('disabled');

        disableBtn(true);

        api.findARoom(null, function(data){     
            disableBtn(false);
            $scope.$apply(function(){
                $location.path('/play/' + data.id);
            });
        });
    };

    $scope.startAnimation = _.debounce(function(){
        var cups  = $element.find('.game li');
        var sec = 0.5;

        //one move
        TweenLite.to(cups[1], sec, { y:'35px', x:'-75px', 'zIndex':1});
        TweenLite.to(cups[0], sec, { y:'0px', x:'160px', 'zIndex':0, delay:sec});
        TweenLite.to(cups[1], sec, { y:'0px', x:'-160px','zIndex':0, delay:sec*2});

        //secound move
        TweenLite.to(cups[0], sec, { y:'35px', x:'75px', 'zIndex':1, delay:sec*3});
        TweenLite.to(cups[1], sec, { y:'0px', x:'0px', 'zIndex':0, delay: sec*4});
        TweenLite.to(cups[0], sec, { y:'0px', x:'0px', 'zIndex':1, delay: sec*5, onComplete:function(){
            var random = _.random(0, 1);
            animateCup(cups[random]);
        }});
    }, 0);

   function disableBtn(arg_b){
        var btn = $element.find('.startPlay');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    }

    function animateCup(arg_li){
        var li = angular.element(arg_li);

        var bitcoin = li.find('.bitcoin');
        var cup = li.find('.cup');
        var id = li.attr('id');
        var sec = 0.3;

        var moveX = (id === 'a')? '-30px' : '30px';
        var rotate = (id === 'a')? '-15' : '15';

        TweenLite.to(cup, sec, {y:'-80px', x: moveX, rotation: rotate, delay: sec});
        TweenLite.to(cup, sec, {y:'0px', x: 0, rotation: 0, delay:sec*7, onComplete:function(){
            $scope.startAnimation();
        }});
        
        TweenLite.fromTo(bitcoin, 0, { opacity:0 }, { opacity: 1, display:'block', delay: 0 });
    } 
})

;