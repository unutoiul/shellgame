angular.module('app')

.controller('SupportCtrl', function ($scope, $element, api) {

    var disableBtn = function(arg_b){
        var btn = $element.find('.button');
        if(arg_b){
            btn.addClass('disabled loading');
        }else{
            btn.removeClass('disabled loading');
        }
    };

    $scope.$evalAsync(function(){
    	$scope.supportView = 'send';

    });


	$scope.sendMessage = function(message, valid){
		if(!valid) return;

		disableBtn(true);

		api.sendMessage(message, function(err, data){
			disableBtn(false);
			if(err){
				$scope.$evalAsync(function(){
					$scope.error = err;
				});
			}

		  	$scope.$evalAsync(function(){
		    	$scope.supportView = 'successful';
	    	});


			console.log('data ', data);
		});
	};
})

;