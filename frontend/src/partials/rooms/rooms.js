angular.module('app')

.controller('RoomsCtrl',function($scope, $location, userData, api) {

	$scope.$on('update', function(event, data){
		if(!data.online){
			$scope.$apply(function(){
				$scope.rooms = data;
			});
		}
	});

	// api.getRooms(function(data){
	// 	$scope.$apply(function(){
	// 		$scope.rooms = data;
	// 	});
	// });

	$scope.createGame = function(){
		//create a rooom
		var data = {
	    	amount: Math.random(),
	    	players: {},
	    	games: {}
	    };

		api.createRoom(data, function(data){     		
	    	$scope.$apply(function(){
	    		$location.path('/play/'+data.id);
	    	});
		});
	};

	$scope.join = function(roomId){
		//join a rooom
		api.joinRoom(roomId, function(err, data){
			if(err){
				$scope.$apply(function(){
					$scope.error = err;
	    		});
	    		return err;	
			}

    		$scope.$apply(function(){
	    		$location.path('/play/'+roomId);
	    	});
		});
	};
});
