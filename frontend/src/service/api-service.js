angular.module('app')

.service('api', function($rootScope, $resource, websocket, envService) {
	var config = envService.read();
	var baseUrl = config.http + config.domain + config.port + config.api;

	var api = {
		login: 	$resource(baseUrl + '/login', {'username':'@username', 'password':'@password'}, {
			post: {method: "POST"},
		}),
		join: $resource(baseUrl + '/join', {}, {
			post: { method: "POST", isArray: false},
		}),
	    createAccount: $resource(baseUrl + '/create-account', {}, {
			post: { method: "POST", isArray: false},
		}),  	    
		countAccounts: $resource(baseUrl + '/count', {}, {
			get: { method: "GET", isArray: false},
		}),    

		getChatMessages: function(callback){
			websocket.call('/api/messages', 'GET', null, function(err, data){
			 	if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
			});
		},	


		getAccount: function(callback){
			websocket.call('/api/account', 'GET', null, function(err, data){
			 	if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
			});
		},

		updateAccount: function(data, callback){
			websocket.call('/api/account', 'PUT', data, function(err, data){
		    	callback(err, data);
			});
		},

		getTransactions: function(status, callback){
			websocket.call('/api/transactions/'+status, 'GET', null, function(err, data){
			 	if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
			});
		},

		addTransaction: function(data, callback){
			websocket.call('/api/transactions', 'POST', data, function(err, data){
	    		callback(err, data);
			});
		},

		connect: function(auth, callback){
			websocket.connect(auth, function(data){
		        callback(data);
			});
		},	
		disconnected: function(roomId, callback){
			websocket.call('/api/disconnected/'+roomId, 'GET', null, function(err, data){
		    	callback(err, data);
		 	});
		},

		disconnect: function(callback){
			websocket.disconnect(function(){
		        callback();
			});
		},

		getRoom: function(id, callback){
			websocket.call('/api/room/'+id, 'GET', null, function(err, data){
			 	if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
			});
		},
		
		getRooms: function(callback){
			websocket.call('/api/rooms', 'GET', null, function(err, data){
			 	if(err){
			 		throw new Error(err);
			 	}
			 	callback(data);
			});
		},

		sendMessage: function(data, callback){
		 	websocket.call('/api/support', 'POST', data, function(err, data){
		    	callback(err, data);
		    });
		},			

		createRoom: function(data, callback){
		 	websocket.call('/api/create-room', 'POST', data, function(err, data){
		    	if(err){
	    			throw new Error(err);
		    	}
		    	callback(data);
		    });
		},	

		findARoom: function(data, callback){
		 	websocket.call('/api/find-a-room', 'POST', data, function(err, data){
		    	if(err){
	    			throw new Error(err);
		    	}
		    	callback(data);
		    });
		},
		
		joinRoom: function(roomId, callback){
			websocket.call('/api/join-room/'+roomId, 'GET', null, function(err, data){
		    	callback(err, data);
		 	});
		},
		leaveRoom: function(roomId, callback){
			websocket.call('/api/leave-room/'+roomId, 'GET', null, function(err, data){
		    	callback(err, data);
		 	});
		},

		gameAction: function(roomId, data, callback){
			websocket.call('/api/game/'+roomId, 'PUT', data, function(err, data){
		    	callback(err, data);
		 	});
		},	

		swapPlayers: function(roomId, callback){
			websocket.call('/api/swapPlayers/'+roomId, 'GET', null, function(err, data){
		    	callback(err, data);
		 	});
		},

		faucet: function(data, callback){
			websocket.call('/api/faucet', 'POST', data, function(err, data){
			 	if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
			});
		},	

		sendChatMessage: function(data, callback){
			websocket.call('/api/messages', 'PUT', data, function(err, data){
				callback(err, data);
			});
		},		

		sendMessageToServer: function(data, callback){
			websocket.sendMessage(data, function(err, data){
		 		if(err){
		    		throw new Error(err);
		    	}
		    	callback(data);
		 	});
		},

		subscribe: function(name, update, callback){
		 	websocket.subscribe(name, update, callback);
		}

	};
	return api;
})

;