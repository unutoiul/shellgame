angular.module('app')

.service('authentication', function(api, userData) {

    return {
        checkToken: function(token) {
            if(!userData.getToken()) return;

            var checkToken = api.checkToken.get();
            checkToken.$promise
            .then(function(data){
                if(data.statusCode === 401){
                    userData.removeToken();
                    // return;
                    // $rootScope.logged = false;
                }
                console.log(data);
            })
            .catch(function(event){
                console.log(event);
            });
        }
    };
});
