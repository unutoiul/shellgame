angular.module('app')

.service('interceptor', function($rootScope, userData) {
    var service = this;

    service.request = function(config) {
        var token = userData.getToken();
        if (token) {
            config.headers.authorization = token;
        }
        return config;
    };

    // service.response = function (data) {
    //     console.log('response in interceptor', data);
    // };

    service.responseError = function(response) {
        if (response.status === 401) {
            $rootScope.$broadcast('unauthorized');
        }
        return response;
    };
})

;