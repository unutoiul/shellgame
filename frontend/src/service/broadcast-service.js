angular.module('app')

.service('broadcast', function($rootScope) {
    return {
        event: function (name, data) {
            $rootScope.$broadcast(name, data);
        }
       
    };
});
