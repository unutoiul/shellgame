angular.module('app')

.service('counter', function($interval, envService) {
    var interval;

    var counter = {
        start: function(callback){
            interval = $interval(callback, 1000);
        },
        stop: function(){
            $interval.cancel(interval);
        }
    };

    return counter;
});
