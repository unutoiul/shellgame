angular.module('app')

.service('userData', function($cookies) {
    var token = null;
    var socketId = null;
    var data = null;
    var connected = false;
    // var online = null;

    return {
        setConnection: function(b){
            connected = b;
        },

        connected: function(){
            return connected;
        },

        setToken: function(arg_token) {
            token = arg_token;
            $cookies.put('x-token', arg_token);
            
            return token;
        },
        
        getToken: function() {
            token = (token) ? token : $cookies.get('x-token');
            if (!token) {
                return null;
            }
            return token;
        },

        removeToken: function(){
            token = null;
            $cookies.remove('x-token');
        },

        setSocketId: function(arg_socket_id){
            socketId = arg_socket_id;
        },

        socketId: function() {
            return socketId;
        },

        setData: function(arg_data){
            data = arg_data;
        },
        
        getData: function(){
            return data;
        }

        // updateOnline: function(arg_online){
            // online = arg_online;

            // console.log('updateOnline');
            // return online;
            // console.log('updateOnlineUsers', arg_online)
            // $rootScope.$emit('updateOnlineUsers', arg_online);
        // }, 

        // getOnline: online;
    };
});
