angular.module('app')

.factory('regex', function() {
	return {
        email: new RegExp('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$'),
        bitcoinAddress: new RegExp('^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$'),
        bitcoinAmount: new RegExp('^[0-9]+(\.[0-9]{1,8})?$')
    };
});