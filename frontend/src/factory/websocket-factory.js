var Nes = require('nes');

angular.module('app')

.factory('websocket', function(broadcast, events, userData, envService) {
    var config = envService.read();
	var ws = config.ws + config.domain + config.port;
    
    console.log('ws', ws);

    var client = new Nes.Client(ws);

    client.onConnect = function () {
        var socketId = client.id.split(':')[0];
        userData.setSocketId(socketId);
        userData.setConnection(true);

        broadcast.event(events.login, {connected:true});
        broadcast.event(events.popup, {close: true});

    };
    client.onUpdate = function (event) {
        // console.log('onUpdate', event);
        broadcast.event(event.name, event.data);
    };

    client.onDisconnect = function(willReconnect, log){
        if(willReconnect){
            userData.setConnection(false);
            userData.setConnection(null);
            broadcast.event(events.login, {connected:false});
            broadcast.event(events.popup, {popup: 'disconnected', tab:null, closable: false});
        }
    };
    
    client.onError = function(){

    };

    return {
        subscriptions : function(){
            return client.subscriptions();
        },

        connect: function(auth, callback){
            client.connect(auth, function(err){
                if(err){
                   return callback(err);
                }
                var socketId = client.id.split(':')[0];
                callback({socketId: socketId});
            });
        },        

        disconnect: function(callback){
            client.disconnect(function(data){
                callback(data);
            });
        },

        subscribe: function(path, handler, callback){
            client.subscribe(path, handler, callback);
        },      

        unsubscribe: function(path, handler, callback){
            client.unsubscribe(path, handler, callback);
        },

        update: function(callback){
            client.onUpdate = function (arg_payload) {
                callback(arg_payload);
            };
        },

        call: function(path, method, message, callback){
            client.request({
                path: path,
                method: method,
                payload: message,
            }, callback);
        },

        sendMessage: function(data, callback) {
            client.message(data, callback);
        }
    };
})

;   
