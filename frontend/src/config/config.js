angular.module('app')
.config(function(envServiceProvider) {
	envServiceProvider.config({
		domains: {
				development: ['localhost'],
				production: ['shellgame.net'],
		},
		vars: {
			development: {
				ws: 'ws://',
				http: 'http://',
				domain: 'localhost',
				port: ':8000',
				api: '/api',
				disconnected: 5,
				recaptcha: {
					siteKey: '6LeZACgUAAAAAPye-jM_0BhLVk2qsei98-jnEf6z',
				}
			},
			production: {
				ws: 'wss://',
				http: 'https://',	
				domain: 'shellgame.net',
				port: '',
				api: '/api',
				disconnected: 30,
				recaptcha: {
					siteKey: '6LfuEygUAAAAAPnaN6pAq4ywWtngESIpzZgbHk2U',
				}
			}
		},
	});

	envServiceProvider.check();
});