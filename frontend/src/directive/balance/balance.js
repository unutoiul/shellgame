angular.module('app')
.directive('balance', function () {
  	return {
	    templateUrl: 'assets/directive/balance/balance.html',
        controller: 'BalanceCtrl'
	    // restrict: 'A',
  	};
})

.controller('BalanceCtrl', function($scope, $element, broadcast, userData, api, vars, events) {
	$element.find('.dropdown').dropdown();

    $scope.$on(events.login, function(event, data){
        if(data.connected){
            api.getAccount(function(data){
                broadcast.event(events.updateUser, data);
                userData.setData(data);
            });
        }
    });

    $scope.$on(events.updateUser, function(event, data){
        updateData(data);
        startAni();
    });

    $scope.getToken = function() {
        return userData.getToken();
    };

    $scope.loadPopup = function(popup, tab) {
        broadcast.event(events.popup, {popup: popup, tab:tab});
    };

	$scope.logout = function() {
        api.disconnect(function(){
            userData.removeToken();
            broadcast.event(events.login, { connected:false });
            broadcast.event(events.popup, { popup: 'startup', tab:'login', closable: false});
        });
	};

    function startAni(){
        var totalAmount = $element.find('.totalAmount');
        var sec = 0.2;

        TweenLite.to(totalAmount, sec, {scale:1.1});
        TweenLite.to(totalAmount, sec, {scale:1, delay: sec});
    }
    function updateData(data){
        $scope.$evalAsync(function() {
            $scope.userData = data;
        });

        userData.setData(data);
    }
});