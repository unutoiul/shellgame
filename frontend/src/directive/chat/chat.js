angular.module('app')

.directive('chat', function () {
  	return {
	    templateUrl: 'assets/directive/chat/chat.html',
	    controller: 'ChatCtrl'
	    // restrict: 'A'
  	};
})

.controller('ChatCtrl', function($scope, $element, $timeout, api, vars, events) {
	$scope.data = [];
	$scope.chatOpen = false;

 	api.subscribe('/mainChat', updateChat, function(err){
 		if(err){
 			throw new Error(err);
 		}
 	});

 	$scope.$on(events.login, function(event, data){
 		if(!data.connected) return;

       	api.getChatMessages(function(messages){
			$scope.$evalAsync(function(){
				$scope.data = messages;
		    });

		    // console.log('userLogin ', messages);

		    $timeout(function(){
		    	animateScroll();
			});
       	});
    });

	$scope.submit = function(newMessage){
	 	if (!newMessage) { return; }
	 	
	 	$scope.newMessage = '';

 		var message = {
 			message: newMessage,
 			time: new Date()
 		};

 		api.sendChatMessage(message, function(data){
 			// console.log('sendChatMessage', data);
 		});

		// $scope.subscriptions = websocket.subscriptions();
	};

	$scope.chatToogle = function(){
		$scope.chatOpen = !$scope.chatOpen;
		
		// var left = ($scope.chatOpen)? '-300px':'0px';
		// var marginLeft = ($scope.chatOpen)? '0px':'300px';
		// var ease = ($scope.chatOpen)? 'easeOut':'easeIn';
		// $element.find('#chat').animate({'left': left}, 200);
		// angular.element('#view').animate({'marginLeft': marginLeft}, 200);
	};

	function updateChat(data, flags){
		$scope.$evalAsync(function(){
		 	$scope.data.push(data);
	    });

		animateScroll();
	}

	function animateScroll() {
		var messages = $element.find('.messages');
		messages.animate({ scrollTop: messages.prop("scrollHeight")}, 500);
	}
})

;