angular.module('app')
.directive('popup', function () {
  	return {
	    templateUrl: 'assets/directive/popup/popup.html',
	    controller: 'PopupCtrl'
  	};
})

.controller('PopupCtrl', function($scope, $element, userData, broadcast, vars, events) {
    var pop = $element.find('.ui.modal');


    $scope.$on(events.popup, function(event, data){
        // console.log('data', data);
        if(data.close){
            var pop = $element.find('.ui.modal');
            pop.modal('hide');
            return;
        }

        $scope.$evalAsync(function(){
           $scope.popup = data.popup;
            $scope.tab = data.tab;
            $scope.closable = data.closable;
        });
 
    });

    $scope.openPopup = function() {
        var pop = $element.find('.ui.modal');

        pop
        .modal({
            detachable: false,
            autofocus: true,
            observeChanges: true,
            closable:  $scope.closable,
            onShow: function(event){
                var tab = angular.element(this).find("[data-tab='" + $scope.tab + "']");
                tab.addClass('active');
            },
            onHidden: function(){
                $scope.$evalAsync(function(){
                    $scope.popup = null;
                });
            }
        })
        .modal('show');
    };


    //init
    if(!userData.connected()){
        broadcast.event(events.popup, {popup: 'startup', tab: 'welcome', closable: false});
    }
})

;