angular.module('app')
.directive('online', function () {
  	return {
	    templateUrl: 'assets/directive/online/online.html',
	    // restrict: 'A',
	    controller: 'OnlineCtrl'
  	};
})

.controller('OnlineCtrl', function($scope, userData, api) {
	$scope.$on('online', function(event, data){
		$scope.$evalAsync(function(){
			$scope.roomsOnline = data.rooms;
			$scope.usersOnline = data.users;
		});
	});
})

;