angular.module('app')
.directive('header', function () {
  	return {
	    templateUrl: 'assets/directive/header/header.html',
	    // restrict: 'A',
	    controller: 'HeaderCtrl'
  	};
})

.controller('HeaderCtrl', function($scope) {
	// console.log('MainMenuCtrl');

	// $scope.getToken = function() {
	// 	return userData.getToken();
	// };
	
	// $scope.logout = function(){
	// 	userData.removeToken();
	// };

	$scope.logout = function(){
		userData.removeToken();
	};

})

;