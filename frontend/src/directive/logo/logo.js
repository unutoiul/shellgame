angular.module('app')
.directive('logo', function () {
  	return {
	    templateUrl: 'assets/directive/logo/logo.html',
        controller: 'LogoCtrl',
        scope: {
            type: '@'
        }
  	};
})

.controller('LogoCtrl', function($scope, $element) {
    $element.children().addClass($scope.type);
});