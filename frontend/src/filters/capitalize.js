angular.module('app')

.filter('capitalize', function() {
  	return function(str) {
      	return str.charAt(0).toUpperCase() + str.slice(1);
   	};
});