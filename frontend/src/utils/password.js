angular.module('app')

.directive("compare", function () {
    return {
        require: "ngModel",
        scope: {
            confim: "=compare"
        },
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.compare = function(value) {
                return value == scope.confim;
            };
            scope.$watch("confirm", function() {
                ngModel.$validate();
            });
        }
    };
});