angular.module('app')
.constant('vars', {
	gameStatus: {
		'player:waiting': 'Waiting for player to join...',
		'player:connected': 'Please select a cup.'  ,
		'player:disconnect': 'Waiting for player to reconnect.' ,
		'player:disconnected': '[Player Name] was disconnected.',
		'game:start': '[Player Name] is selecting.',
		'game:end': '[Player Name] is won 1 btc.',
	},
	errors:{
		'form': 'There is an error validating the form.'
	}

});