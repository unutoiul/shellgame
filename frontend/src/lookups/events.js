angular.module('app')
.constant('events', {
	login: "userLogin",
	popup: "loadPopup",
	updateUser: "updateUser",
	updateConnection: "updateConnection"
	
});